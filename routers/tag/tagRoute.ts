import express from 'express';
import tagController from '../../controllers/admin/tagController';
import validationTag from "../../middleware/tag.validation";
export const tagRoute = express.Router();
tagRoute.get('/', tagController.index);
tagRoute.post('/add', validationTag.validateBase, tagController.create);
tagRoute.get('/:id', tagController.getOne);
tagRoute.put('/update/:id',validationTag.validateBase, tagController.update);
tagRoute.delete('/delete/:id', tagController.delete);