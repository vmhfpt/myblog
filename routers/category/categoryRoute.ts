import express from 'express';
import categoryController from '../../controllers/admin/categoryController';
import validationCategory from "../../middleware/category.validation";
export const categoryRoute = express.Router();
categoryRoute.get('/', categoryController.index);
categoryRoute.post('/add', validationCategory.validateBase, categoryController.create);
categoryRoute.get('/:id', categoryController.getOne);
categoryRoute.put('/update/:id',validationCategory.validateBase, categoryController.update);
categoryRoute.delete('/delete/:id', categoryController.delete);