import express from 'express';
import userController from '../../controllers/admin/userController';
import  validationUser from "../../middleware/user.validation";
export const userRoute = express.Router();
userRoute.get('/', userController.index);
userRoute.post('/add',validationUser.validateBase,  userController.create);
userRoute.get('/:id', userController.getOne);
userRoute.put('/update/:id',validationUser.validateBase, userController.update);
userRoute.delete('/delete/:id', userController.delete);