import express, { Application } from 'express';
import { userRoute } from './user/userRoute';
import { categoryRoute } from './category/categoryRoute';
import { tagRoute } from './tag/tagRoute';
import { postRoute } from './post/postRoute';
import auth from '../authentication/auth';
import  validationUser from "../middleware/user.validation";
import valiationComment from "../middleware/comment.validation";
import userController from '../controllers/admin/userController';
import authLogin from '../middleware/authentication';
import { upload } from '../middleware/multer';
import { uploadImage } from '../config/cloudinary';

import categoryController from '../controllers/post/categoryController';
import postController from '../controllers/post/postController';
import commentController from '../controllers/post/commentController';
export default function useRoute(app : Application){
    app.get('/' , (req : any, res : any) => {
        return res.send('Hello nodejs');
    })
    app.use('/admin/users',authLogin('admin'), userRoute);
    app.use('/admin/categories',authLogin('admin'), categoryRoute);
    app.use('/admin/tags', authLogin('admin'), tagRoute);
    app.use('/admin/posts', authLogin('admin'), postRoute);
    app.post('/upload' ,upload.single('upload'), async (req : any, res : any) => {
        const dataImage = await uploadImage(req.file.path, 'ckeditor');
        return res.json({ url: dataImage.url });
    })


    app.post('/login',validationUser.validateLogin, auth.login);
    app.post('/logout', validationUser.validateRefreshToken, auth.logout);
    app.post('/register',validationUser.validateRegister, auth.register);
    app.post('/refresh-token',validationUser.validateRefreshToken,userController.getNewAccessToken);

    app.get('/api/category', categoryController.getAllCategoriesExceptChildId);
    app.post('/api/post-comment',valiationComment.validateBase, commentController.insert);
    app.get('/api/home', postController.getHome);
    app.get('/api/all-posts', postController.getAllPost);
    app.get('/api/get-search', postController.getBySearch);
    app.get('/api/typing-keyboard', postController.getByAutoComplete);
    app.get('/api/comments/:id', postController.getAllComments);
    app.get('/api/:slug', postController.getDetail);
    app.get('/api/category/:slug', postController.getPostByCategory);
    app.get('/api/tag/:slug', postController.getPostByTag);
    

}