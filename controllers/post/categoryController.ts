import { Request, Response } from 'express';
import categoryService from '../../services/categoryService';
class CategoryController {
     public async index(req : Request, res : Response){
          try {
               const dataItem = await categoryService.getAll();
               return res.status(200).json(dataItem);
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     public async getAllCategoriesExceptChildId(req : Request, res : Response){
          try {
               const dataItem = await categoryService.getAllExceptChildId();
               return res.status(200).json(dataItem);
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     
     
}
export default new CategoryController;