import { Request, Response } from 'express';
import commentService from '../../services/commentService';
import { CommentAttributes } from '../../models/comment/comment.interface';
class CommentController {
     public async insert(req : Request, res : Response){
       try {
            const payload : CommentAttributes = {
                ...req.body,
                user_id : req.body.user_id == 0 ? null : req.body.user_id
            }
            const dataInsert = await commentService.insert(payload);
            return res.status(201).json(dataInsert);
       } catch (error : any) {
            return res.status(500).json({error : error.errors[0].message});
       }
     }
     
}
export default new CommentController;