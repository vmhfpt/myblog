import { Request, Response } from 'express';
import postService from '../../services/postService';
import categoryService from '../../services/categoryService';
import tagService from '../../services/tagService';
import { PostAttributes } from '../../models/post/post.interface';
import commentService from '../../services/commentService';
interface Post {
   post_id : number;
}
class PostController {
     public async getAllPost(req: Request, res : Response){
        try {
           const dataItem = await postService.getAllPost();
           return res.status(200).json(dataItem);
        } catch (error) {
            return res.status(500).json({status : 'error', error});
        }
     }
     public static getIdProduct(dataItem : Post[]) : number[]{
         return dataItem.map((item) => {
               return Number(item.post_id);
         });
     }

     public static async getNavData(dataItem : number[] = []){
        return Promise.all([
            categoryService.getAllExceptChildId(),
            tagService.getAll(),
            postService.getPostNotInId(dataItem , 3),
            commentService.getCommentSuggest()
        ])
        .then(([categoryAll, tag, postSuggest, commentSuggest]) => {
            return {
                categoryAll,
                tag,
                postSuggest,
                commentSuggest
            }
        })
     }

     public async getHome(req: Request, res : Response){
        return Promise.all([
            categoryService.getCategoryAndPost(),
            categoryService.findOneCategoryAndGetPost(2, 5),
            categoryService.findOneCategoryAndGetPost(3, 4),
            categoryService.findOneCategoryAndGetPost(4, 4),
            categoryService.findOneCategoryAndGetPost(15, 4)
        ])
        .then(async ([rowFirst, rowSecond, rowThird, rowFour, rowFive]) => {
            var dataIdArr: number[] = [];
            let tempArr: number[] = rowFirst.map((item: any) => {
                return Number(item.post_categories[0].post_id);
            });
            dataIdArr.push(...tempArr);
            dataIdArr.push(...PostController.getIdProduct(rowSecond));
            dataIdArr.push(...PostController.getIdProduct(rowThird));
            dataIdArr.push(...PostController.getIdProduct(rowFour));
            dataIdArr.push(...PostController.getIdProduct(rowFive));
            const randomPost = await postService.getPostNotInId(dataIdArr, 17);

            dataIdArr.push(...randomPost.map((item) => {
                return Number(item.id)
            }));
            
            const dataNav = await PostController.getNavData(dataIdArr);
            const rowRandom = randomPost.slice(0, 6);
            const randomFirst = randomPost.slice(6, 9);
            const randomSecond = randomPost.slice(9, 12);
            const randomSlider =  randomPost.slice(12);
            return res.json({
                rowFirst,
                rowSecond,
                rowThird,
                rowFour,
                rowFive,
                rowRandom,
                randomFirst,
                randomSecond,
                randomSlider,
                dataNav : dataNav 
            });
        })

        
     }
     public async getDetail(req: Request, res : Response){
         
         const dataItem : PostAttributes | any = await postService.getOneBySlug((String(req.params.slug)));
         if(!dataItem){
            return res.status(404).json({message : 'Post not found'});
         }else {
            var dataIdInit : number[] = [Number(dataItem.id)];
            const postSuggestByCategory = await postService.getPostSuggestByCategory(dataItem.categories[0].id, dataItem.id);
            dataIdInit.push(...postSuggestByCategory.map((item) => {
                return Number(item.post_id);
            }));
            const postSuggestByTag = await postService.getPostSuggestByTag(Number(dataItem.tags[0].id), dataIdInit);
            dataIdInit.push(...postSuggestByTag.map((item) => {
                return Number(item.post_id);
            }));
            const dataNav = await PostController.getNavData(dataIdInit);
            return res.json({dataItem, postSuggestByCategory, postSuggestByTag, dataNav} );
         }
     }
     public async getPostByCategory(req: Request, res : Response){
        let arrIdInit : number[] = [];
        const limit_item = 6;
        const slug : string = String(req.params.slug);
        const page : number = req.query.page ? Number(req.query.page) : 1;
        const category = await categoryService.findOneBySlug(slug);
        if(!category) return res.status(404).json({message : 'Category not found'});
        const result = await postService.getPostByCategory(slug, page, limit_item);        
        arrIdInit.push(...result.map((item : any) => {
            return Number(item.post_id)
        }));
        return Promise.all([
            postService.findAllProductByCategoryId(category.id),
            PostController.getNavData(arrIdInit)
        ])
        .then(([total, dataNav]) => {
            return res.json({
                dataNav,
                category,
                result ,
                paginate : {
                   total_item : total.count,
                   current_page : page,
                   next_page : page < Math.ceil(total.count / limit_item) ? page + 1 : false,
                   prev_page : page - 1 <= 0 ? false : page - 1,
                   total_page : total.count / limit_item > 0 ?  Math.ceil(total.count / limit_item)  : 0 ,
                   limit_item : limit_item,
                   more_item : total.count -  result.length > 0 ? total.count -  result.length : false,
                }
            });
        })
       

        
     }
     public async getPostByTag(req: Request, res : Response){
        let arrIdInit : number[] = [];
        const limit_item = 6;
        const slug : string = String(req.params.slug);
        const page : number = req.query.page ? Number(req.query.page) : 1;
        const tag = await tagService.findOneBySlug(slug);
        if(!tag) return res.status(404).json({message : 'Category not found'});
        const result = await postService.getPostByTag(slug, page, limit_item);
        
        arrIdInit.push(...result.map((item : any) => {
            return Number(item.post_id)
        }));
        
        return Promise.all([
            postService.findAllProductByTagId(tag.id),
            PostController.getNavData(arrIdInit)
        ])
        .then(([total, dataNav]) => {
            return res.json({
                dataNav,
                tag,
                result ,
                paginate : {
                   total_item : total.count,
                   current_page : page,
                   next_page : page < Math.ceil(total.count / limit_item) ? page + 1 : false,
                   prev_page : page - 1 <= 0 ? false : page - 1,
                   total_page : total.count / limit_item > 0 ?  Math.ceil(total.count / limit_item)  : 0 ,
                   limit_item : limit_item,
                   more_item : total.count -  result.length > 0 ? total.count -  result.length : false,
                }
            });
        })
        
     }
     public async getByAutoComplete(req: Request, res : Response){
         const dataItem = await postService.getByAutoComplete(String(req.query.key));
         return res.status(200).json(dataItem);
     }
     public async getBySearch(req: Request, res : Response){
        let arrIdInit : number[] = [];
        const limit_item = 6;
        const page : number = req.query.page ? Number(req.query.page) : 1;
        const result = await postService.getPostByTitle(String(req.query.key), page, limit_item);
        arrIdInit.push(...result.map((item : any) => {
            return Number(item.id);
        }));
        return Promise.all([
            postService.getAllPostByTitle(String(req.query.key)),
            PostController.getNavData(arrIdInit)
        ])
        .then(([total, dataNav]) => {
            return res.json({
                dataNav,
                result ,
                paginate : {
                   total_item : total.count,
                   current_page : page,
                   next_page : page < Math.ceil(total.count / limit_item) ? page + 1 : false,
                   prev_page : page - 1 <= 0 ? false : page - 1,
                   total_page : total.count / limit_item > 0 ?  Math.ceil(total.count / limit_item)  : 0 ,
                   limit_item : limit_item,
                   more_item : total.count -  result.length > 0 ? total.count -  result.length : false,
                }
            });
        })
     }
     public async getAllComments(req: Request, res : Response){
         const dataItem = await postService.getAllCommentsById(Number(req.params.id));
         if(dataItem) return res.status(200).json(dataItem);
         return  res.status(200).json([]);
     }
}
export default new PostController;