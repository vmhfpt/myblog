import { Request, Response } from 'express';
import postService from '../../services/postService';
import post_tagService from '../../services/post_tagService';
import post_categoryService from '../../services/post_categoryService';
import post_metaService from '../../services/post_metaService';
import { PostAttributes } from '../../models/post/post.interface';
import { PostTagAttributes } from '../../models/post_tag/post_tag.interface';
import { PostCategoryAttributes } from '../../models/post_category/post_category.interface';
import { PostMetaAttributes } from '../../models/post_meta/post_meta.interface';
import { uploadImage } from '../../config/cloudinary';
import { deleteImage } from '../../config/cloudinary';
import slug from 'slug';
class PostController {
     public async index(req : Request, res : Response){
          try {
               const dataItem = await postService.getAll();
               return res.status(200).json(dataItem);
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     
     public async getOne(req : Request, res : Response){
          
          try {
               const id : number = Number(req.params.id);
               const dataItem = await postService.findOneById(id);
               if(dataItem) return res.status(200).json(dataItem);
               return res.status(404).json({status : 'error', message : 'Post not found'});
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     public async create(req : Request, res : Response){
          const imagePath : string  = req.file ? req.file.path : '';
          const dataImage = await uploadImage(imagePath, "posts");
          let payload : PostAttributes = {
               ...req.body,
               thumb : dataImage.url ? dataImage.url : 'https://laptrinhcuocsong.com/images/thumbnail_default.png',
               slug : slug(req.body.title)
          }
          const dataInsert = await postService.insert(payload);
          
          return Promise.all([
               post_tagService.insert({ post_id : dataInsert.id, tag_id : req.body.tag_id } as PostTagAttributes),
               post_categoryService.insert({ post_id : dataInsert.id, category_id : req.body.category_id } as PostCategoryAttributes),
               post_metaService.insert({ post_id : dataInsert.id, key : dataInsert.title } as PostMetaAttributes)
          ])
          .then(async ([]) => {
               const data = await postService.findOneById(dataInsert.id);
               return res.status(201).json(data);
          })
          // try {
          //      const imagePath : string  = req.file ? req.file.path : '';
          //      const dataImage = await uploadImage(imagePath, "posts");
          //      let payload : PostAttributes = {
          //           ...req.body,
          //           thumb : dataImage.url ? dataImage.url : 'https://laptrinhcuocsong.com/images/thumbnail_default.png',
          //           slug : slug(req.body.title)
          //      }
          //      const dataInsert = await postService.insert(payload);
               
          //      return Promise.all([
          //           post_tagService.insert({ post_id : dataInsert.id, tag_id : req.body.tag_id } as PostTagAttributes),
          //           post_categoryService.insert({ post_id : dataInsert.id, category_id : req.body.category_id } as PostCategoryAttributes),
          //           post_metaService.insert({ post_id : dataInsert.id, key : dataInsert.title } as PostMetaAttributes)
          //      ])
          //      .then(async ([]) => {
          //           const data = await postService.findOneById(dataInsert.id);
          //           return res.status(201).json(data);
          //      })
          // } catch (error : any) {
          //      return res.status(500).json({error : error.errors[0].message});
          // }
     }
     public async update(req : Request, res : Response){
          const dataItem  = await postService.findOneById(Number(req.params.id));
          if(!dataItem) return res.status(404).json({status : 'error', message : 'Post not found'});
          let payload = {
               ...req.body,
               slug : slug(req.body.title),
          }
          const temp : any = dataItem;
          return Promise.all([
               post_metaService.findOneAndUpdate({key : req.body.title} as PostMetaAttributes, Number(temp.post_metum.id)),
               post_categoryService.findOneAndUpdate({category_id : req.body.category_id} as PostCategoryAttributes, Number(temp.categories[0].post_categories.id)),
               post_tagService.findOneAndUpdate({ tag_id : req.body.tag_id} as PostTagAttributes, Number(temp.tags[0].post_tags.id) ),
               postService.findOneAndUpdate(payload, Number(req.params.id))
          ])
          .then(async ([]) => {
               return Promise.all([
                    deleteImage(dataItem.thumb, req.file ? true : false, "posts"),
                    uploadImage(req.file ? req.file.path : false, "posts")
               ])
               .then(async ([dataDelete, dataUpload]) => {
                    await postService.findOneAndUpdate({thumb : dataUpload.status == 'error' ? dataItem.thumb : dataUpload.url} as PostAttributes, Number(req.params.id));
                    const getData = await postService.findOneById(Number(req.params.id));
                    return res.status(200).json({status : 'success', data : getData});
               })
               .catch((error) => {
                    return res.status(500).json({status : 'error', error});
               })
          })
          .catch((error) => {
               return res.status(500).json({error : error.errors[0].message});
          })

     }
     public async delete(req : Request, res : Response){
          try {
               const id : number = Number(req.params.id);
               const dataItem = await postService.findOneById(id);
               if(!dataItem) return res.status(404).json({status : 'error', message : 'Post not found'});
               const temp : any = dataItem;
               return Promise.all([
                    deleteImage(dataItem.thumb, true, "posts"),
                    post_tagService.destroy(Number(temp.tags[0].post_tags.id)),
                    post_categoryService.destroy(Number(temp.categories[0].post_categories.id)),
                    post_metaService.destroy(Number(temp.post_metum.id)),
                    postService.destroy(dataItem.id)
               ])
               .then(([]) => {
                    return res.status(200).json({status : 'delete success'});
               })
               .catch((error) => {
                    return res.status(500).json({status : 'Error internal server', error});
               })
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
}
export default new PostController;