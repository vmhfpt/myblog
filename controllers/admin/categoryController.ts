import { Request, Response } from 'express';
import categoryService from '../../services/categoryService';
import { CategoryAttributes } from '../../models/category/category.interface';
import slug from 'slug';
class CategoryController {
     public async index(req : Request, res : Response){
          try {
               const dataItem = await categoryService.getAll();
               return res.status(200).json(dataItem);
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     
     public async getOne(req : Request, res : Response){
          try {
               const id : number = Number(req.params.id);
               const dataItem = await categoryService.findOneById(id);
               if(dataItem) return res.status(200).json(dataItem);
               return res.status(404).json({status : 'error', message : 'Category not found'});
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     public async create(req : Request, res : Response){
          try {
               const payload : CategoryAttributes = {
                    ...req.body,
                    slug : slug(req.body.title)
               };
               const dataInsert = await categoryService.insert(payload);
               return res.status(201).json(dataInsert);
          } catch (error : any) {
               return res.status(500).json({error : error.errors[0].message});
          }
     }
     public async update(req : Request, res : Response){
          try {
               const payload : CategoryAttributes = {
                  ...req.body,
                  slug : slug(req.body.title)
               };
               const id : number = Number(req.params.id);
               const dataUpdate = await categoryService.findOneAndUpdate(payload, id);
               if(dataUpdate) return res.status(200).json(dataUpdate);
               return res.status(404).json({status : 'error', message : 'Category not found'});
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
     public async delete(req : Request, res : Response){
          const id : number = Number(req.params.id);
          try {
               const dataDelete = await categoryService.destroy(id);
               if(dataDelete) return res.status(200).json({status : 'delete success'});
               return res.status(404).json({status : 'error', message : 'Category not found'});
          } catch (error) {
               return res.status(500).json({status : 'Error internal server', error});
          }
     }
}
export default new CategoryController;