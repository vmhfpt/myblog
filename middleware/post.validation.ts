import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import ValidationRequest from './validationRequest';

class ValidationPost extends ValidationRequest {
    public validateBase(req: Request, res: Response, next: NextFunction){
        
        const schema = Joi.object({
            ...ValidationPost.handleTitle,
            ...ValidationPost.handleUserId,
            ...ValidationPost.handleDescription,
            ...ValidationPost.handleActive,
            ...ValidationPost.handleContent,
            ...ValidationPost.handleCategoryId,
            ...ValidationPost.handleTagId
        });
        return ValidationPost.validateResult(schema.validate(req.body),res, next);
    }
    public validateUpdate(req: Request, res: Response, next: NextFunction){
        
        const schema = Joi.object({
            ...ValidationPost.handleTitle,
            ...ValidationPost.handleDescription,
            ...ValidationPost.handleActive,
            ...ValidationPost.handleContent,
            ...ValidationPost.handleCategoryId,
            ...ValidationPost.handleTagId
        });
        return ValidationPost.validateResult(schema.validate(req.body),res, next);
    }
    public validateKeySearch(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
            ...ValidationPost.handleKey,
        });
        return ValidationPost.validateResult(schema.validate(req.query),res, next);
    }
    
}

export default new ValidationPost;