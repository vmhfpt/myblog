import {  verify } from 'jsonwebtoken';
import 'dotenv/config';
import { Request, Response, NextFunction} from 'express';
export default function authLogin(role = 'admin'){
    
   return (req : Request, res : Response, next : NextFunction) => {
      //   next();
      //   return true;
      const authHeader = req.header('authorization');
      const token = authHeader && authHeader.split(' ')[1];
      if(token == null){
         return res.status(403).json({status : 'error' , message : 'Token is empty or invalid'});
      }
      try {
         const decoded : any = verify(token, process.env.ACCESS_TOKEN_SECRET as string);
         
         if(decoded.data.role == 0 && role == 'admin'){
            return res.status(403).json({status : 'error' , message : 'You don`t have authorization'});
         }
         next();
      }catch {
         return  res.status(401).json({status : 'error', message : 'Unauthorized'});
      } 
   }
  
}
