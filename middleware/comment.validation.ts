import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import ValidationRequest from './validationRequest';
class ValidationComment extends ValidationRequest {
    public validateBase(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
           ...ValidationComment.handleActive,
           ...ValidationComment.handleParentId,
           ...ValidationComment.handleUserId,
           ...ValidationComment.handleName,
           ...ValidationComment.handleEmail,
           ...ValidationComment.handleContent,
           ...ValidationComment.handlePostId
        });
        return ValidationComment.validateResult(schema.validate(req.body),res, next);
    }
    
}

export default new ValidationComment;