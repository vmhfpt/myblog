import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import ValidationRequest from './validationRequest';
class ValidationUser extends ValidationRequest {
    public validateBase(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
           ...ValidationUser.handleName,
           ...ValidationUser.handleEmail,
           ...ValidationUser.handlePassword,
           ...ValidationUser.handleRole,
           ...ValidationUser.handleLoginType
        });
        return ValidationUser.validateResult(schema.validate(req.body),res, next);
    }
    public validateLogin(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
            ...ValidationUser.handleEmail,
            ...ValidationUser.handlePassword,
         });
         return ValidationUser.validateResult(schema.validate(req.body),res, next);
    }
    public validateRegister(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
            ...ValidationUser.handleEmail,
            ...ValidationUser.handlePassword,
            ...ValidationUser.handleName,
        });
        return ValidationUser.validateResult(schema.validate(req.body),res, next);
    }
    public validateRefreshToken(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
            ...ValidationUser.handleRefreshToken
        });
        return ValidationUser.validateResult(schema.validate(req.body),res, next);
    }
    
}

export default new ValidationUser;