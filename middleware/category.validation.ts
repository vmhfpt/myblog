import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import ValidationRequest from './validationRequest';
class ValidationCategory extends ValidationRequest {
    public validateBase(req: Request, res: Response, next: NextFunction){
        const schema = Joi.object({
           ...ValidationCategory.handleActive,
           ...ValidationCategory.handleParentId,
           ...ValidationCategory.handleTitle
        });
        return ValidationCategory.validateResult(schema.validate(req.body),res, next);
    }
    
}

export default new ValidationCategory;