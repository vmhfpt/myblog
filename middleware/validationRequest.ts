import { Response, NextFunction } from 'express';
import Joi, { ValidationResult } from 'joi';

class ValidationRequest {
    public static readonly handleTagId : any = { tag_id : Joi.number().required().messages({
        'number.base': 'Tag id must be a number.',
        'number.empty': 'Tag id is required.',
        'any.required': 'Tag id is required.',
    })};
    public static readonly handlePostId : any = { post_id : Joi.number().required().messages({
        'number.base': 'Post id must be a number.',
        'number.empty': 'Post id is required.',
        'any.required': 'Post id is required.',
    })};
    public static readonly handleCategoryId : any = { category_id : Joi.number().required().messages({
        'number.base': 'Category id must be a number.',
        'number.empty': 'Category id is required.',
        'any.required': 'Category id is required.',
    })};
    public static readonly handleParentId : any = { parent_id : Joi.number().required().messages({
        'number.base': 'Parent id must be a number.',
        'number.empty': 'Parent id is required.',
        'any.required': 'Parent id is required.',
    })};
    public static readonly handleUserId : any = { user_id : Joi.number().required().messages({
        'number.base': 'Author id must be a number.',
        'number.empty': 'Author id is required.',
        'any.required': 'Author id required.',
    })};
    public static readonly handleName : any = { name: Joi.string().required().messages({
        'string.empty': 'Name is required.',
        'any.required': 'Name is required.',
    })};
    public static readonly handleDescription : any = { description: Joi.string().required().messages({
        'string.empty': 'Description is required.',
        'any.required': 'Description is required.',
    })};
    public static readonly handleContent : any = { content: Joi.string().required().messages({
        'string.empty': 'Content is required.',
        'any.required': 'Content is required.',
    })};
    public static readonly handleTitle : any = { title: Joi.string().required().messages({
        'string.empty': 'Title is required.',
        'any.required': 'Title is required.',
    })};
    public static readonly handleKey : any = { key: Joi.string().required().messages({
        'string.empty': 'Key is required.',
        'any.required': 'Key is required.',
    })};
    public static readonly handleActive : any = { active: Joi.number().min(0).max(1).required().messages({
        'number.empty': 'Active is required.',
        'any.required': 'Active is required.',
        'number.base': 'Active must be a number.',
        'number.max': 'Active  must be a number 0 or 1',
        'number.min': 'Active  must be a number 0 or 1',
    })};
    public static readonly handleEmail : any = { email: Joi.string().email().required().messages({
        'string.email': 'Invalid email format.',
        'string.empty': 'Email is required.',
        'any.required': 'Email is required.',
    })};
    public static readonly handlePassword : any = { password : Joi.string().required().min(6).max(20).messages({
        'string.empty': 'Password is required.',
        'any.required': 'Password is required.',
        'string.max': 'Password length must be less than or equal to 20 characters long',
        'string.min': 'password length must be at least 6 characters long',
    })};
    public static readonly handleRole : any = { role : Joi.number().required().messages({
        'number.base': 'Role must be a number.',
        'number.empty': 'Role is required.',
        'any.required': 'Role is required.',
    })};
    public static readonly handleRefreshToken : any = { refresh_token : Joi.string().min(10).required().messages({
        'string.empty': 'Refresh token is required.',
        'any.required': 'Refresh token is required.',
        'string.min': 'Refresh token must be at least {#limit} characters long.',
    })};
    public static readonly handleLoginType : any = { login_type : Joi.number().min(1).max(3).required().messages({
        'number.base': 'Login type must be a number.',
        'number.empty': 'Login type is required.',
        'any.required': 'Login type is required.',
        'number.max': 'Login type  must be a number from 1 to 3',
        'number.min': 'Login type  must be a number from 1 to 3',
    })};
    public static readonly handleImage : any = {
        image: Joi.object({
            data: Joi.binary().required(),
            mimetype: Joi.string().valid('image/jpeg', 'image/png', 'image/gif', 'image/jpg').required(),
        }).required(),
    }



    protected static validateResult(validation : ValidationResult,res: Response, next: NextFunction){
        if (validation.error) {
            return res.status(400).json({ error: validation.error.details[0].message });
        } else {
            next();
        }
    }
    
}
export default ValidationRequest;