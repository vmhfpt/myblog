export interface UserAttributes {
    id : number;
    name: string;
    role : number;
    password: string;
    email: string;
    remember_token : string;
    google_id : string;
    facebook_id : string;
    login_type : number;
}