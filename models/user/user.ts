import { DataTypes, Model, Sequelize } from 'sequelize';
import { UserAttributes } from './user.interface';
import { sequelize } from '../../config/mysql';
import { hash} from 'bcrypt';
interface UserCreationAttributes extends UserAttributes {}

export class User extends Model<UserAttributes, UserCreationAttributes> implements UserAttributes {
    id!: number;
    name!: string;
    password!: string;
    role!: number;
    email!: string;
    google_id!: string;
    facebook_id!: string;
    login_type!: number;
    remember_token !: string;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  User.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      role: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
      },
      google_id: {
        type: DataTypes.STRING,
        allowNull: true,
        unique: true,
      },
      facebook_id: {
        type: DataTypes.STRING,
        allowNull: true,
        unique: true,
      },
      login_type: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
      },
      remember_token: {
        type: DataTypes.STRING,
        allowNull: true,
        unique: false,
      },
    },
    {
      hooks: {
        beforeCreate: async (user) => {
          const hashedPassword = await hash(user.password, 10);
          user.password = hashedPassword;
        },
        beforeUpdate: async (user) => {
          const hashedPassword = await hash(user.password, 10);
          user.password = hashedPassword;
        },
      },
      sequelize,
      modelName: 'users',
    }
  );