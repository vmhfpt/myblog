import { DataTypes, Model } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { PostCategoryAttributes } from './post_category.interface';
interface PostCategoryCreationAttributes extends PostCategoryAttributes  {}

export class PostCategory extends Model<PostCategoryAttributes , PostCategoryCreationAttributes> implements PostCategoryAttributes  {
    id!: number;
    post_id!: number;
    category_id!: number;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  PostCategory.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      post_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      category_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
     
    },
    {
      timestamps: false,
      sequelize,
      modelName: 'post_categories',
    }
  );