export interface PostCategoryAttributes {
    id : number;
    post_id: number;
    category_id : number;
}