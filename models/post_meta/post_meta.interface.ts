export interface PostMetaAttributes {
    id : number;
    post_id: number;
    key : string;
}