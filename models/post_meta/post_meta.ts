import { DataTypes, Model } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { PostMetaAttributes } from './post_meta.interface';
interface PostMetaCreationAttributes extends PostMetaAttributes {}

export class PostMeta extends Model<PostMetaAttributes, PostMetaCreationAttributes> implements PostMetaAttributes {
    id!: number;
    post_id!: number;
    key!: string;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  PostMeta.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      post_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      key: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    },
    {
      sequelize,
      modelName: 'post_meta',
    }
  );