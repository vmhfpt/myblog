export interface PostTagAttributes {
    id : number;
    post_id: number;
    tag_id : number;
}