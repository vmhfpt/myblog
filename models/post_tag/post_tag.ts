import { DataTypes, Model } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { PostTagAttributes } from './post_tag.interface';
import { Tag } from '../tag/tag';
import { Post } from '../post/post';
interface PostTagCreationAttributes extends PostTagAttributes  {}

export class PostTag extends Model<PostTagAttributes , PostTagCreationAttributes> implements PostTagAttributes  {
    id!: number;
    post_id!: number;
    tag_id!: number;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  PostTag.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      post_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      tag_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
     
    },
    {
      timestamps: false,
      sequelize,
      modelName: 'post_tags',
    }
  );


  