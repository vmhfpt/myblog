import { DataTypes, Model, Sequelize } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { CommentAttributes } from './comment.interface';
interface CategoryCreationAttributes extends CommentAttributes  {}

export class Comment extends Model<CommentAttributes , CategoryCreationAttributes> implements CommentAttributes  {
    id !: number;
    user_id!: number;
    post_id !: number;
    parent_id !: number;
    name !: string;
    email!: string;
    content !: string;
    active !: number;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  Comment.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      post_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      user_id: {
        type: DataTypes.INTEGER,
      },
      parent_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      content: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      active: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'comments',
    }
  );
 