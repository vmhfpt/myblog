export interface CommentAttributes {
    id : number;
    user_id: number;
    post_id : number;
    parent_id : number;
    name : string;
    email: string;
    content : string;
    active : number;
}