export interface CategoryAttributes {
    id : number;
    parent_id: number;
    title: string;
    slug: string;
    active : number;
}