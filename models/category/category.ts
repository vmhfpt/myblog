import { DataTypes, Model, Sequelize } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { CategoryAttributes } from './category.interface';
import { Post } from '../post/post';
import { PostCategory } from '../post_category/post_category';
interface CategoryCreationAttributes extends CategoryAttributes {}

export class Category extends Model<CategoryAttributes, CategoryCreationAttributes> implements CategoryAttributes {
    id!: number;
    parent_id!: number;
    title!: string;
    slug!: string;
    active!: number;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  Category.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      parent_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      slug: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      active: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
      },
    },
    {
      sequelize,
      modelName: 'categories',
    }
  );
 