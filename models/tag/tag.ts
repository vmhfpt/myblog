import { DataTypes, Model, Sequelize } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { TagAttributes } from './tag.interface';
import { Post } from '../post/post';
import { PostTag } from '../post_tag/post_tag';
interface TagCreationAttributes extends TagAttributes {}

export class Tag extends Model<TagAttributes, TagCreationAttributes> implements TagAttributes {
    id!: number;
    title!: string;
    slug!: string;
    // You can add any custom methods or class-level methods here.
    static associate(models: any) {
      // Define associations here if needed.
    }
  }
  Tag.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      slug: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },

    },
    {
      sequelize,
      modelName: 'tags',
    }
  );
  // Tag.belongsToMany(Post, {
  //     through: PostTag,
  //     foreignKey : 'tag_id'
  // });