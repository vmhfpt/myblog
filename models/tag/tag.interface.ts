export interface TagAttributes {
    id : number;
    title: string;
    slug: string;
}