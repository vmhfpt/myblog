export interface PostAttributes {
    
    id : number;
    user_id: number;
    title: string;
    slug: string;
    description : string;
    content : string;
    thumb : string;
    active : number;
}