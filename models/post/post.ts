import { DataTypes, Model, Sequelize } from 'sequelize';
import {sequelize} from "../../config/mysql";
import { PostAttributes } from './post.interface';
import { User } from '../user/user';
import { Tag } from '../tag/tag';
import { PostTag } from '../post_tag/post_tag';
import { PostMeta } from '../post_meta/post_meta';
import { Category } from '../category/category';
import { PostCategory } from '../post_category/post_category';
import { Comment } from '../comment/comment';
interface PostCreationAttributes extends PostAttributes {}

export class Post extends Model<PostAttributes, PostCreationAttributes> implements PostAttributes {
    id!: number;
    user_id!: number;
    title!: string;
    slug!: string;
    description!: string;
    content!: string;
    thumb!: string;
    active!: number;
    // You can add any custom methods or class-level methods here.
    // static associate(models: any) {
    //   Post.belongsTo(models.User, {
    //      foreignKey: 'user_id'
    //   });
    // }
  }
  Post.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      slug: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      content: {
        type: DataTypes.TEXT('long'),
        allowNull: false,
      },
      thumb: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      active: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
      },
    },
    {
      sequelize,
      modelName: 'posts',
    }
  );

  Post.belongsTo(User, {
      foreignKey: 'user_id'
  });
  Post.belongsToMany(Tag, {
      through: PostTag,
      foreignKey : 'post_id'
  });
  Tag.belongsToMany(Post, {
    through: PostTag,
    foreignKey : 'tag_id'
  });
  Post.hasOne(PostMeta, {
    foreignKey : 'post_id'
  });
  Post.belongsToMany(Category, {
    through: PostCategory,
    foreignKey : 'post_id'
  });
  Category.belongsToMany(Post, {
    through: PostCategory,
    foreignKey : 'category_id'
  });
  Category.hasMany(PostCategory, {
    foreignKey : 'category_id'
  })
  Category.belongsTo(Category, {
    foreignKey: 'parent_id'
})

  PostTag.belongsTo(Tag, {
    foreignKey: 'tag_id'
  });
  PostTag.belongsTo(Post, {
    foreignKey: 'post_id'
  });


  PostCategory.belongsTo(Category, {
    foreignKey: 'category_id'
  });
  PostCategory.belongsTo(Post, {
    foreignKey: 'post_id'
  });

  Comment.belongsTo(Post, {
    foreignKey  : "post_id"
  })
  Comment.hasMany(Comment, {
   foreignKey  : "parent_id",
   as : 'child_comments',
  })
  Post.hasMany(Comment, {
    foreignKey : 'post_id',
    as : 'post_comments'
  });