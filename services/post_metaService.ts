import { PostMetaAttributes } from "../models/post_meta/post_meta.interface";
import { PostMeta } from "../models/post_meta/post_meta";
class PostMetaService {
    public async getAll(): Promise<PostMetaAttributes[]> {
         return await PostMeta.findAll({});
    }
    public async findOneAndUpdate(payload : PostMetaAttributes, id : number) : Promise<PostMetaAttributes  | null>{
        const postMeta = await PostMeta.findByPk(id);
        if(PostMeta){
            await PostMeta.update(payload, {
                where: {
                  id: id
                }
            });
            return postMeta;
        }
        return null;
    }
    public async insert(payload : PostMetaAttributes) : Promise<PostMetaAttributes> {
         return await PostMeta.create(payload);
    }
    public async findOneById(id : number) : Promise<PostMetaAttributes  | null>  {
         return await PostMeta.findOne({ where: { id : id } });
    }
    
    public async destroy(id : number) :  Promise<PostMetaAttributes  | null> {
        const postMeta = await PostMeta.findByPk(id);
        if(PostMeta){
            await PostMeta.destroy({
                where: {
                    id : id
                }
            });
            return postMeta;
        }
        return null;
    }
}
export default new PostMetaService;