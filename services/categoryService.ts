import { CategoryAttributes } from "../models/category/category.interface";
import { Category } from "../models/category/category";
import { Post } from "../models/post/post";
import { PostCategory } from "../models/post_category/post_category";
import { User } from "../models/user/user";
import sequelize from "sequelize";
class CategoryService {
    public async getAllExceptChildId(){

        return await PostCategory.findAll({
            attributes: [
                [sequelize.fn('COUNT', sequelize.col('category_id')), 'total'],
            ],
            include : {
                model : Category,
                attributes: ['title', 'slug', 'id'],
            },
            group: ['`post_categories`.`category_id`'],
        });
       
    }
    public async getCategoryAndPost(){
        return await Category.findAll({
            
            include: [
                {
                    model: PostCategory,
                    include : [{
                        model : Post,
                        attributes: ['createdAt','title', 'description', 'thumb', 'slug'],
                        include : [{
                            model : User,
                            attributes: ['name'],
                        }]
                    }],
                    
                    limit: 1 ,
                    order: [['id', 'DESC']],
                },
            ],
            attributes: ['title', 'slug']

        }); 
    }
    public async findOneCategoryAndGetPost(category_id : number, limit : number){
        return await PostCategory.findAll({
            offset : 1,
            limit : limit,
            where: {
                category_id: category_id
            },
            include : [
                {
                    model : Post,
                    attributes: ['createdAt','title', 'description', 'thumb', 'slug'],
                    include : [{
                        model : User,
                        attributes: ['name'],
                    }]
                },
                {
                    model : Category,
                    attributes: ['id', 'title', 'slug'],
                }
            ],
            order: [['id', 'DESC']],

        });
        
    }

    public async getAll(): Promise<CategoryAttributes[]> {
         return await Category.findAll({
            include: {
                model: Category,
                attributes : ['title', 'id']
            }
         });
    }
    public async findOneAndUpdate(payload : CategoryAttributes, id : number) : Promise<CategoryAttributes  | null>{
        const category = await Category.findByPk(id);
        if(category){
            await Category.update(payload, {
                where: {
                  id: id
                }
            });
            return category;
        }
        return null;
    }
    public async insert(payload : CategoryAttributes) : Promise<CategoryAttributes> {
         return await Category.create(payload);
    }
    public async findOneById(id : number) : Promise<CategoryAttributes  | null>  {
         return await Category.findOne({ where: { id : id } });
    }
    public async findOneBySlug(slug : string) {
        return await Category.findOne({
            where : {
                slug : slug
            },
            attributes : ['id', 'title', 'slug'],
        });
    }
    
    public async destroy(id : number) :  Promise<CategoryAttributes  | null> {
        const category = await Category.findByPk(id);
        if(category){
            await Category.destroy({
                where: {
                    id : id
                }
            });
            return category;
        }
        return null;
    }
}
export default new CategoryService;