import { UserAttributes } from "../models/user/user.interface";
import { User } from "../models/user/user";
class UserService {
    public async getAll(): Promise<UserAttributes[]> {
        return await User.findAll({});
    }
    public async findOneAndUpdate(payload : UserAttributes, id : number) : Promise<UserAttributes  | null>{
        const user = await User.findByPk(id);
        if(user){
            user.name = payload.name;
            user.email = payload.email;
            user.role = payload.role;
            user.login_type = payload.login_type;
            user.password = payload.password;
            await user.save();
            return user;
        }
        return null;
    }
    public async insert(payload : UserAttributes) : Promise<UserAttributes> {
        return await User.create(payload);
    }
    public async findOneById(id : number) : Promise<UserAttributes  | null>  {
        return await User.findOne({ where: { id : id } });
    }
    public async login(email : string) : Promise<UserAttributes  | null>{
        return await User.findOne({ where: { email : email } });
    }
    public async findByIdAndUpdateToken(id : number, payload : {remember_token : string}) : Promise<UserAttributes  | null>{
        const user = await User.findByPk(id);
        if(user){
            await User.update(payload , {
                where: {
                  id : id
                }
            });
            return user;
        }
        return null;
    }
    public async destroy(id : number) :  Promise<UserAttributes  | null> {
        const user = await User.findByPk(id);
        if(user){
            await User.destroy({
                where: {
                    id : id
                }
            });
            return user;
        }
        return null;
    }
}
export default new UserService;