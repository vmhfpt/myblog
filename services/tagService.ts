import { TagAttributes } from "../models/tag/tag.interface";
import { Tag } from "../models/tag/tag";
class TagService {
    public async getAll(): Promise<TagAttributes[]> {
         return await Tag.findAll({});
    }
    public async findOneAndUpdate(payload : TagAttributes, id : number) : Promise<TagAttributes  | null>{
        const tag = await Tag.findByPk(id);
        if(tag){
            await Tag.update(payload, {
                where: {
                  id: id
                }
            });
            return tag;
        }
        return null;
    }
    public async insert(payload : TagAttributes) : Promise<TagAttributes> {
         return await Tag.create(payload);
    }
    public async findOneById(id : number) : Promise<TagAttributes  | null>  {
         return await Tag.findOne({ where: { id : id } });
    }
    public async findOneBySlug(slug : string) : Promise<TagAttributes  | null> {
        return await Tag.findOne({ where: { slug : slug } });
    }
    
    public async destroy(id : number) :  Promise<TagAttributes  | null> {
        const tag = await Tag.findByPk(id);
        if(tag){
            await Tag.destroy({
                where: {
                    id : id
                }
            });
            return tag;
        }
        return null;
    }
}
export default new TagService;