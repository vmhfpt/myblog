import { PostTagAttributes } from "../models/post_tag/post_tag.interface";
import { PostTag } from "../models/post_tag/post_tag";
import { Tag } from "../models/tag/tag";
import { Post } from "../models/post/post";
import { User } from "../models/user/user";
class PostTagService {
    public async getAll(): Promise<PostTagAttributes[]> {
        // return await PostTag.findAll({});
        const result = await PostTag.findAll({
            include : [
                {
                    model : Tag,
                    attributes : ['id', 'title']
                },
                {
                    model : Post,
                    attributes : ['title','description', 'createdAt', 'slug', 'thumb'],
                    include : [
                        {
                            model : User, //error Object literal may only specify known properties, and 'model' does not exist in type 'Includeable[]'. in typescirpt
                            attributes: ['name']  
                        }
                    ]
                }
            ],
            order: [['id', 'DESC']],
           });
        return result;
    }
    public async findOneAndUpdate(payload : PostTagAttributes, id : number) : Promise<PostTagAttributes  | null>{
        const postTag = await PostTag.findByPk(id);
        if(postTag){
            await PostTag.update(payload, {
                where: {
                  id: id
                }
            });
            return postTag;
        }
        return null;
    }
    public async insert(payload : PostTagAttributes) : Promise<PostTagAttributes> {
         return await PostTag.create(payload);
    }
    public async findOneById(id : number) : Promise<PostTagAttributes  | null>  {
         return await PostTag.findOne({ where: { id : id } });
    }
    
    public async destroy(id : number) :  Promise<PostTagAttributes  | null> {
        const postTag = await PostTag.findByPk(id);
        if(postTag){
            await PostTag.destroy({
                where: {
                    id : id
                }
            });
            return postTag;
        }
        return null;
    }
}
export default new PostTagService;