import { PostCategoryAttributes } from "../models/post_category/post_category.interface";
import { PostCategory } from "../models/post_category/post_category";
class PostCategoryService {
    public async getAll(): Promise<PostCategoryAttributes[]> {
         return await PostCategory.findAll({});
    }
    public async findOneAndUpdate(payload : PostCategoryAttributes, id : number) : Promise<PostCategoryAttributes  | null>{
        const post = await PostCategory.findByPk(id);
        if(post){
            await PostCategory.update(payload, {
                where: {
                  id: id
                }
            });
            return post;
        }
        return null;
    }
    public async insert(payload : PostCategoryAttributes) : Promise<PostCategoryAttributes> {
         return await PostCategory.create(payload);
    }
    public async findOneById(id : number) : Promise<PostCategoryAttributes  | null>  {
         return await PostCategory.findOne({ where: { id : id } });
    }
    
    public async destroy(id : number) :  Promise<PostCategoryAttributes  | null> {
        const post = await PostCategory.findByPk(id);
        if(post){
            await PostCategory.destroy({
                where: {
                    id : id
                }
            });
            return post;
        }
        return null;
    }
}
export default new PostCategoryService;