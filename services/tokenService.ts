import { sign, verify, Secret } from 'jsonwebtoken';
import 'dotenv/config';
import userService from './userService';
import { UserAttributes } from '../models/user/user.interface';
interface TokenPayload {
    id: number;
    name: string;
    email: string;
    role: number;
}
class TokenService {
    private static readonly refreshTokenSecret : Secret = process.env.REFRESH_TOKEN_SECRET as string;
    private static readonly timeRefreshTokenExpired : string = process.env.TIME_REFRESH_TOKEN_EXPIRED as string;

    private static readonly accessTokenSecret : Secret = process.env.ACCESS_TOKEN_SECRET as string;
    private static readonly timeAccessTokenExpired : string = process.env.TIME_ACCESS_TOKEN_EXPIRED as string;


    public generateRefreshToken(data : TokenPayload) : string {
        const refresh_token = sign({
            data: data
        }, TokenService.refreshTokenSecret , { expiresIn: TokenService.timeRefreshTokenExpired });
        return refresh_token;
    }
    public generateAccessToken(data : TokenPayload) : string {
        const access_token = sign({
            data: data
        }, TokenService.accessTokenSecret, { expiresIn: TokenService.timeAccessTokenExpired });
        return access_token;
    }
    public async verifyTokenUser(id : number){
        try {
            const dataUser : UserAttributes | null = await userService.findOneById(id);
            if(dataUser){
                const tokenVerify = dataUser.remember_token;
                if(!tokenVerify ) throw new Error('unauthoziration');
                verify(tokenVerify, TokenService.refreshTokenSecret) as TokenPayload;
            }
        } catch (error) {
            throw new Error('unauthoziration');
        }
    }
    public verifyRefreshTokenUser(refreshToken : string) : number {
        const decoded : any = verify(refreshToken, TokenService.refreshTokenSecret) as TokenPayload;
        return decoded.data.id;
    }
    public async getNewAccessToken(refreshToken : string){
        
        try {
            const decoded : any = verify(refreshToken, TokenService.refreshTokenSecret) as TokenPayload;
            const {id, name, email, role} : TokenPayload = decoded.data;
            await this.verifyTokenUser(id);
            const access_token = this.generateAccessToken({id, name, email, role});
            const refresh_token = this.generateRefreshToken({id, name, email, role});
            return ({status : 'success', access_token, refresh_token});
        }catch {
            return ({status : 'error', message : 'refresh_token error'})
        } 
    }

}
export default new TokenService;