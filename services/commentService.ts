import { CommentAttributes } from "../models/comment/comment.interface";
import { Comment } from "../models/comment/comment";
import { Post } from "../models/post/post";
class CommentService {
   
    public async insert(payload : CommentAttributes) : Promise<CommentAttributes> {
         return await Comment.create(payload);
    }
    public async getCommentSuggest(){
        return  Comment.findAll({
            offset : 0,
            limit: 3 ,
            include : {
                model : Post,
                attributes: ['slug']
            },
            order: [['id', 'DESC']],
            attributes: ['name', 'content', 'user_id']
        }); 
    }
    
}
export default new CommentService;