import { PostAttributes } from "../models/post/post.interface";
import { Post } from "../models/post/post";
import { User } from "../models/user/user";
import { Tag } from "../models/tag/tag";
import { PostMeta } from "../models/post_meta/post_meta";
import { Category } from "../models/category/category";
import { Op } from "sequelize";
import { PostCategory } from "../models/post_category/post_category";
import { PostTag } from "../models/post_tag/post_tag";
import { Comment } from "../models/comment/comment";
import { CategoryAttributes } from "../models/category/category.interface";
import { CommentAttributes } from "../models/comment/comment.interface";
class PostService {
    public async getPostNotInId(dataItem : number[], limit : number){
        return await Post.findAll({
            limit: limit ,
            order: [['id', 'DESC']],
            attributes: ['createdAt','title', 'thumb', 'slug', 'id'],
            where: {
              id: {
                [Op.notIn]: dataItem,
              },
            },
        })
    }
    public async getAll(): Promise<PostAttributes[]> {
         return await Post.findAll({
            include: [
                {
                    model: Tag,
                    attributes : ['id','title', 'slug'],
                    
                },
                {
                    model : PostMeta
                },
                {
                    model : Category,
                    attributes : ['title', 'slug', 'id'],
                    through: {}
                },
                {
                    model : User,
                    attributes : ['name', 'id']
                }
            ],
            order: [["id", "DESC"]],
         });
    }
    public async findOneAndUpdate(payload : PostAttributes, id : number) : Promise<PostAttributes  | null>{
        const post = await Post.findByPk(id);
        if(post){
            await Post.update(payload, {
                where: {
                  id: id
                }
            });
            return post;
        }
        return null;
    }
    public async insert(payload : PostAttributes) : Promise<PostAttributes> {
         return await Post.create(payload);
    }
    public async findOneById(id : number) : Promise<PostAttributes  | null>  {
         let dataItem  = await Post.findOne({ 
            where: { id : id },
            include: [
                {
                    model: Tag,
                    attributes : ['title', 'slug', 'id'],
                    
                },
                {
                    model : PostMeta
                },
                {
                    model : Category,
                    attributes : ['title', 'slug', 'id'],
                    through: {}
                },
                {
                    model : User,
                    attributes : ['name', 'id']
                }
            ],
         });
         if(dataItem){
            return dataItem;
         }
         return null;
         
    }
    
    public async destroy(id : number) :  Promise<PostAttributes  | null> {
        const post = await Post.findByPk(id);
        if(post){
            await Post.destroy({
                where: {
                    id : id
                }
            });
            return post;
        }
        return null;
    }
    public async getAllCommentsById(id : number) : Promise<CommentAttributes[]  | null>{
        let dataItem  = await Comment.findAll({ 
            where: { post_id : id , parent_id : 0},
            include: [
                {
                    model: Comment,
                    as: 'child_comments', 
                    required: false,
                    order: [['id', 'DESC']],
                },
                // how to order by id in here
            ],
            order: [['id', 'DESC']],
             // how to order by id in here
         });
         if(dataItem.length != 0){
            return dataItem;
         }
         return null;
    }
    public async getOneBySlug(slug : string) : Promise<PostAttributes  | null> {
        let dataItem  = await Post.findOne({ 
            where: { slug : slug },
            include: [
                {
                    model: Tag,
                    attributes : ['title', 'slug', 'id'],
                    
                },
                {
                    model : PostMeta
                },
                {
                    model : Category,
                    attributes : ['title', 'slug', 'id'],
                    through: {}
                },
                {
                    model : User,
                    attributes : ['name', 'id']
                },
            ],
         });
         if(dataItem){
            return dataItem;
         }
         return null;
    }
    public async getAllPost() : Promise<PostAttributes[]  | null>{
        let dataItem  = await Post.findAll({ 
            attributes : ['slug']
         });
         if(dataItem){
            return dataItem;
         }
         return null;
    }
    public async getPostSuggestByCategory(category_id : number, post_id : number){
        return await PostCategory.findAll({
            where: {
                [Op.and]: [{ category_id : category_id  }, { post_id: { [Op.ne]: post_id} }],  
            },
            include : {
                model : Post,
                attributes : ['title','description', 'createdAt', 'slug', 'thumb']
            },
            offset : 0,
            limit: 3 ,
            order: [['id', 'DESC']],
        })
    }
    public async getPostSuggestByTag(tag_id : number, arrayId : number[]){
         return await PostTag.findAll({
            where: {
                [Op.and]: [{ tag_id : tag_id  }, { post_id: { [Op.notIn]: arrayId} }],  
            },
            include : {
                model : Post,
                attributes : ['title','description', 'createdAt', 'slug', 'thumb']
            },
            offset : 0,
            limit: 6 ,
            order: [['id', 'DESC']],
         })
    }
    public async getPostByCategory(slug : string, page : number, limit_item : number){
        return await PostCategory.findAll({
            include : [
                {
                    model : Category,
                    where : {
                        slug : slug
                    },
                    attributes : ['id']
                },
                {
                    model : Post,
                    attributes : ['title','description', 'createdAt', 'slug', 'thumb'],
                    include : [{
                        model : User,
                        attributes: ['name']
                    }]
                },
                
            ],
            offset : 0,
            limit: page *  limit_item ,
            order: [['id', 'DESC']],
        });
    }
    public async getPostByTag(slug : string, page : number, limit_item : number){
        return await PostTag.findAll({
            include : [
                {
                    model : Tag,
                    where : {
                        slug : slug
                    },
                    attributes : ['id']
                },
                {
                    model : Post,
                    attributes : ['title','description', 'createdAt', 'slug', 'thumb'],
                    include : [{
                        model : User,
                        attributes: ['name']
                    }]
                },
                
            ],
            offset : 0,
            limit: page *  limit_item ,
            order: [['id', 'DESC']],
        });
    }
    public async findAllProductByTagId(id : number){
        return await PostTag.findAndCountAll({
            where : {
               tag_id : id,  
            },
        });
    }
    public async findAllProductByCategoryId(id : number){
        return await PostCategory.findAndCountAll({
            where : {
               category_id : id,  
            },
        });
    }
    public async getByAutoComplete(key : string){
        return await Post.findAll({
            where : {
                title : {[Op.substring]: key} 
            },
            offset : 0,
            limit: 5 ,
            order: [['id', 'DESC']],
            attributes : ['title', 'slug']
        })

    }
    public async getAllPostByTitle(key : string){
        return await Post.findAndCountAll({
            where : {
                title : {[Op.substring]: key}  
            },
        });
    }
    public async getPostByTitle(key : string, page : number, limit_item : number){
        return await Post.findAll({
            include : [
                {
                    model : User,
                    attributes: ['name']
                }
            ],
            where : {
                title : {[Op.substring]: key} 
            },
            offset : 0,
            limit: page *  limit_item ,
            order: [['id', 'DESC']],
            attributes :  ['id','title','description', 'createdAt', 'slug', 'thumb']
        })
    }
}
export default new PostService;