"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.tagRoute = void 0;
const express_1 = __importDefault(require("express"));
const tagController_1 = __importDefault(require("../../controllers/admin/tagController"));
const tag_validation_1 = __importDefault(require("../../middleware/tag.validation"));
exports.tagRoute = express_1.default.Router();
exports.tagRoute.get('/', tagController_1.default.index);
exports.tagRoute.post('/add', tag_validation_1.default.validateBase, tagController_1.default.create);
exports.tagRoute.get('/:id', tagController_1.default.getOne);
exports.tagRoute.put('/update/:id', tag_validation_1.default.validateBase, tagController_1.default.update);
exports.tagRoute.delete('/delete/:id', tagController_1.default.delete);
