"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postRoute = void 0;
const express_1 = __importDefault(require("express"));
const postController_1 = __importDefault(require("../../controllers/admin/postController"));
const multer_1 = require("../../middleware/multer");
const post_validation_1 = __importDefault(require("../../middleware/post.validation"));
exports.postRoute = express_1.default.Router();
exports.postRoute.get('/', postController_1.default.index);
exports.postRoute.post('/add', multer_1.upload.single('image'), post_validation_1.default.validateBase, postController_1.default.create);
exports.postRoute.get('/:id', postController_1.default.getOne);
exports.postRoute.put('/update/:id', multer_1.upload.single('image'), post_validation_1.default.validateUpdate, postController_1.default.update);
exports.postRoute.delete('/delete/:id', postController_1.default.delete);
