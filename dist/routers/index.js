"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userRoute_1 = require("./user/userRoute");
const categoryRoute_1 = require("./category/categoryRoute");
const tagRoute_1 = require("./tag/tagRoute");
const postRoute_1 = require("./post/postRoute");
const auth_1 = __importDefault(require("../authentication/auth"));
const user_validation_1 = __importDefault(require("../middleware/user.validation"));
const comment_validation_1 = __importDefault(require("../middleware/comment.validation"));
const userController_1 = __importDefault(require("../controllers/admin/userController"));
const authentication_1 = __importDefault(require("../middleware/authentication"));
const multer_1 = require("../middleware/multer");
const cloudinary_1 = require("../config/cloudinary");
const categoryController_1 = __importDefault(require("../controllers/post/categoryController"));
const postController_1 = __importDefault(require("../controllers/post/postController"));
const commentController_1 = __importDefault(require("../controllers/post/commentController"));
function useRoute(app) {
    app.get('/', (req, res) => {
        return res.send('Hello nodejs');
    });
    app.use('/admin/users', (0, authentication_1.default)('admin'), userRoute_1.userRoute);
    app.use('/admin/categories', (0, authentication_1.default)('admin'), categoryRoute_1.categoryRoute);
    app.use('/admin/tags', (0, authentication_1.default)('admin'), tagRoute_1.tagRoute);
    app.use('/admin/posts', (0, authentication_1.default)('admin'), postRoute_1.postRoute);
    app.post('/upload', multer_1.upload.single('upload'), (req, res) => __awaiter(this, void 0, void 0, function* () {
        const dataImage = yield (0, cloudinary_1.uploadImage)(req.file.path, 'ckeditor');
        return res.json({ url: dataImage.url });
    }));
    app.post('/login', user_validation_1.default.validateLogin, auth_1.default.login);
    app.post('/logout', user_validation_1.default.validateRefreshToken, auth_1.default.logout);
    app.post('/register', user_validation_1.default.validateRegister, auth_1.default.register);
    app.post('/refresh-token', user_validation_1.default.validateRefreshToken, userController_1.default.getNewAccessToken);
    app.get('/api/category', categoryController_1.default.getAllCategoriesExceptChildId);
    app.post('/api/post-comment', comment_validation_1.default.validateBase, commentController_1.default.insert);
    app.get('/api/home', postController_1.default.getHome);
    app.get('/api/all-posts', postController_1.default.getAllPost);
    app.get('/api/get-search', postController_1.default.getBySearch);
    app.get('/api/typing-keyboard', postController_1.default.getByAutoComplete);
    app.get('/api/comments/:id', postController_1.default.getAllComments);
    app.get('/api/:slug', postController_1.default.getDetail);
    app.get('/api/category/:slug', postController_1.default.getPostByCategory);
    app.get('/api/tag/:slug', postController_1.default.getPostByTag);
}
exports.default = useRoute;
