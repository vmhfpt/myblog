"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.categoryRoute = void 0;
const express_1 = __importDefault(require("express"));
const categoryController_1 = __importDefault(require("../../controllers/admin/categoryController"));
const category_validation_1 = __importDefault(require("../../middleware/category.validation"));
exports.categoryRoute = express_1.default.Router();
exports.categoryRoute.get('/', categoryController_1.default.index);
exports.categoryRoute.post('/add', category_validation_1.default.validateBase, categoryController_1.default.create);
exports.categoryRoute.get('/:id', categoryController_1.default.getOne);
exports.categoryRoute.put('/update/:id', category_validation_1.default.validateBase, categoryController_1.default.update);
exports.categoryRoute.delete('/delete/:id', categoryController_1.default.delete);
