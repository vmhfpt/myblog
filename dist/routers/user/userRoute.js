"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRoute = void 0;
const express_1 = __importDefault(require("express"));
const userController_1 = __importDefault(require("../../controllers/admin/userController"));
const user_validation_1 = __importDefault(require("../../middleware/user.validation"));
exports.userRoute = express_1.default.Router();
exports.userRoute.get('/', userController_1.default.index);
exports.userRoute.post('/add', user_validation_1.default.validateBase, userController_1.default.create);
exports.userRoute.get('/:id', userController_1.default.getOne);
exports.userRoute.put('/update/:id', user_validation_1.default.validateBase, userController_1.default.update);
exports.userRoute.delete('/delete/:id', userController_1.default.delete);
