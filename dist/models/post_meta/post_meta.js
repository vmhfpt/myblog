"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostMeta = void 0;
const sequelize_1 = require("sequelize");
const mysql_1 = require("../../config/mysql");
class PostMeta extends sequelize_1.Model {
    // You can add any custom methods or class-level methods here.
    static associate(models) {
        // Define associations here if needed.
    }
}
exports.PostMeta = PostMeta;
PostMeta.init({
    id: {
        autoIncrement: true,
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    post_id: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    },
    key: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    }
}, {
    sequelize: mysql_1.sequelize,
    modelName: 'post_meta',
});
