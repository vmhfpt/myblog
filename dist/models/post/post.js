"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Post = void 0;
const sequelize_1 = require("sequelize");
const mysql_1 = require("../../config/mysql");
const user_1 = require("../user/user");
const tag_1 = require("../tag/tag");
const post_tag_1 = require("../post_tag/post_tag");
const post_meta_1 = require("../post_meta/post_meta");
const category_1 = require("../category/category");
const post_category_1 = require("../post_category/post_category");
const comment_1 = require("../comment/comment");
class Post extends sequelize_1.Model {
}
exports.Post = Post;
Post.init({
    id: {
        autoIncrement: true,
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    user_id: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    slug: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    description: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    content: {
        type: sequelize_1.DataTypes.TEXT('long'),
        allowNull: false,
    },
    thumb: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    active: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        unique: true,
    },
}, {
    sequelize: mysql_1.sequelize,
    modelName: 'posts',
});
Post.belongsTo(user_1.User, {
    foreignKey: 'user_id'
});
Post.belongsToMany(tag_1.Tag, {
    through: post_tag_1.PostTag,
    foreignKey: 'post_id'
});
tag_1.Tag.belongsToMany(Post, {
    through: post_tag_1.PostTag,
    foreignKey: 'tag_id'
});
Post.hasOne(post_meta_1.PostMeta, {
    foreignKey: 'post_id'
});
Post.belongsToMany(category_1.Category, {
    through: post_category_1.PostCategory,
    foreignKey: 'post_id'
});
category_1.Category.belongsToMany(Post, {
    through: post_category_1.PostCategory,
    foreignKey: 'category_id'
});
category_1.Category.hasMany(post_category_1.PostCategory, {
    foreignKey: 'category_id'
});
category_1.Category.belongsTo(category_1.Category, {
    foreignKey: 'parent_id'
});
post_tag_1.PostTag.belongsTo(tag_1.Tag, {
    foreignKey: 'tag_id'
});
post_tag_1.PostTag.belongsTo(Post, {
    foreignKey: 'post_id'
});
post_category_1.PostCategory.belongsTo(category_1.Category, {
    foreignKey: 'category_id'
});
post_category_1.PostCategory.belongsTo(Post, {
    foreignKey: 'post_id'
});
comment_1.Comment.belongsTo(Post, {
    foreignKey: "post_id"
});
comment_1.Comment.hasMany(comment_1.Comment, {
    foreignKey: "parent_id",
    as: 'child_comments',
});
Post.hasMany(comment_1.Comment, {
    foreignKey: 'post_id',
    as: 'post_comments'
});
