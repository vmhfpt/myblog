"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
function default_1(sequelize, DataTypes) {
    class Tag extends sequelize_1.Model {
        // You can add any custom methods or class-level methods here.
        static associate(models) {
            // Define associations here if needed.
        }
    }
    Tag.init({
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        slug: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
    }, {
        sequelize,
        modelName: 'tags',
    });
    return Tag;
}
exports.default = default_1;
