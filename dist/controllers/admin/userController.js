"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userService_1 = __importDefault(require("../../services/userService"));
const tokenService_1 = __importDefault(require("../../services/tokenService"));
class UserController {
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const dataItem = yield userService_1.default.getAll();
                return res.status(200).json(dataItem);
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = Number(req.params.id);
                const dataItem = yield userService_1.default.findOneById(id);
                if (dataItem)
                    return res.status(200).json(dataItem);
                return res.status(404).json({ status: 'error', message: 'User not found' });
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const payload = Object.assign({}, req.body);
                const dataInsert = yield userService_1.default.insert(payload);
                return res.status(201).json(dataInsert);
            }
            catch (error) {
                return res.status(500).json({ error: error.errors[0].message });
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const payload = req.body;
                const id = Number(req.params.id);
                const dataUpdate = yield userService_1.default.findOneAndUpdate(payload, id);
                if (dataUpdate)
                    return res.status(200).json(dataUpdate);
                return res.status(404).json({ status: 'error', message: 'User not found' });
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = Number(req.params.id);
            try {
                const dataDelete = yield userService_1.default.destroy(id);
                if (dataDelete)
                    return res.status(200).json({ status: 'delete success' });
                return res.status(404).json({ status: 'error', message: 'User not found' });
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
    getNewAccessToken(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataItem = yield tokenService_1.default.getNewAccessToken(req.body.refresh_token);
            if (dataItem.status == 'success')
                return res.status(200).json(dataItem);
            return res.status(401).json(dataItem);
        });
    }
}
exports.default = new UserController;
