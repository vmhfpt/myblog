"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const postService_1 = __importDefault(require("../../services/postService"));
const post_tagService_1 = __importDefault(require("../../services/post_tagService"));
const post_categoryService_1 = __importDefault(require("../../services/post_categoryService"));
const post_metaService_1 = __importDefault(require("../../services/post_metaService"));
const cloudinary_1 = require("../../config/cloudinary");
const cloudinary_2 = require("../../config/cloudinary");
const slug_1 = __importDefault(require("slug"));
class PostController {
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const dataItem = yield postService_1.default.getAll();
                return res.status(200).json(dataItem);
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = Number(req.params.id);
                const dataItem = yield postService_1.default.findOneById(id);
                if (dataItem)
                    return res.status(200).json(dataItem);
                return res.status(404).json({ status: 'error', message: 'Post not found' });
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const imagePath = req.file ? req.file.path : '';
            const dataImage = yield (0, cloudinary_1.uploadImage)(imagePath, "posts");
            let payload = Object.assign(Object.assign({}, req.body), { thumb: dataImage.url ? dataImage.url : 'https://laptrinhcuocsong.com/images/thumbnail_default.png', slug: (0, slug_1.default)(req.body.title) });
            const dataInsert = yield postService_1.default.insert(payload);
            return Promise.all([
                post_tagService_1.default.insert({ post_id: dataInsert.id, tag_id: req.body.tag_id }),
                post_categoryService_1.default.insert({ post_id: dataInsert.id, category_id: req.body.category_id }),
                post_metaService_1.default.insert({ post_id: dataInsert.id, key: dataInsert.title })
            ])
                .then(([]) => __awaiter(this, void 0, void 0, function* () {
                const data = yield postService_1.default.findOneById(dataInsert.id);
                return res.status(201).json(data);
            }));
            // try {
            //      const imagePath : string  = req.file ? req.file.path : '';
            //      const dataImage = await uploadImage(imagePath, "posts");
            //      let payload : PostAttributes = {
            //           ...req.body,
            //           thumb : dataImage.url ? dataImage.url : 'https://laptrinhcuocsong.com/images/thumbnail_default.png',
            //           slug : slug(req.body.title)
            //      }
            //      const dataInsert = await postService.insert(payload);
            //      return Promise.all([
            //           post_tagService.insert({ post_id : dataInsert.id, tag_id : req.body.tag_id } as PostTagAttributes),
            //           post_categoryService.insert({ post_id : dataInsert.id, category_id : req.body.category_id } as PostCategoryAttributes),
            //           post_metaService.insert({ post_id : dataInsert.id, key : dataInsert.title } as PostMetaAttributes)
            //      ])
            //      .then(async ([]) => {
            //           const data = await postService.findOneById(dataInsert.id);
            //           return res.status(201).json(data);
            //      })
            // } catch (error : any) {
            //      return res.status(500).json({error : error.errors[0].message});
            // }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataItem = yield postService_1.default.findOneById(Number(req.params.id));
            if (!dataItem)
                return res.status(404).json({ status: 'error', message: 'Post not found' });
            let payload = Object.assign(Object.assign({}, req.body), { slug: (0, slug_1.default)(req.body.title) });
            const temp = dataItem;
            return Promise.all([
                post_metaService_1.default.findOneAndUpdate({ key: req.body.title }, Number(temp.post_metum.id)),
                post_categoryService_1.default.findOneAndUpdate({ category_id: req.body.category_id }, Number(temp.categories[0].post_categories.id)),
                post_tagService_1.default.findOneAndUpdate({ tag_id: req.body.tag_id }, Number(temp.tags[0].post_tags.id)),
                postService_1.default.findOneAndUpdate(payload, Number(req.params.id))
            ])
                .then(([]) => __awaiter(this, void 0, void 0, function* () {
                return Promise.all([
                    (0, cloudinary_2.deleteImage)(dataItem.thumb, req.file ? true : false, "posts"),
                    (0, cloudinary_1.uploadImage)(req.file ? req.file.path : false, "posts")
                ])
                    .then(([dataDelete, dataUpload]) => __awaiter(this, void 0, void 0, function* () {
                    yield postService_1.default.findOneAndUpdate({ thumb: dataUpload.status == 'error' ? dataItem.thumb : dataUpload.url }, Number(req.params.id));
                    const getData = yield postService_1.default.findOneById(Number(req.params.id));
                    return res.status(200).json({ status: 'success', data: getData });
                }))
                    .catch((error) => {
                    return res.status(500).json({ status: 'error', error });
                });
            }))
                .catch((error) => {
                return res.status(500).json({ error: error.errors[0].message });
            });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = Number(req.params.id);
                const dataItem = yield postService_1.default.findOneById(id);
                if (!dataItem)
                    return res.status(404).json({ status: 'error', message: 'Post not found' });
                const temp = dataItem;
                return Promise.all([
                    (0, cloudinary_2.deleteImage)(dataItem.thumb, true, "posts"),
                    post_tagService_1.default.destroy(Number(temp.tags[0].post_tags.id)),
                    post_categoryService_1.default.destroy(Number(temp.categories[0].post_categories.id)),
                    post_metaService_1.default.destroy(Number(temp.post_metum.id)),
                    postService_1.default.destroy(dataItem.id)
                ])
                    .then(([]) => {
                    return res.status(200).json({ status: 'delete success' });
                })
                    .catch((error) => {
                    return res.status(500).json({ status: 'Error internal server', error });
                });
            }
            catch (error) {
                return res.status(500).json({ status: 'Error internal server', error });
            }
        });
    }
}
exports.default = new PostController;
