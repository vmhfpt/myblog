"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const postService_1 = __importDefault(require("../../services/postService"));
const categoryService_1 = __importDefault(require("../../services/categoryService"));
const tagService_1 = __importDefault(require("../../services/tagService"));
const commentService_1 = __importDefault(require("../../services/commentService"));
class PostController {
    getAllPost(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const dataItem = yield postService_1.default.getAllPost();
                return res.status(200).json(dataItem);
            }
            catch (error) {
                return res.status(500).json({ status: 'error', error });
            }
        });
    }
    static getIdProduct(dataItem) {
        return dataItem.map((item) => {
            return Number(item.post_id);
        });
    }
    static getNavData(dataItem = []) {
        return __awaiter(this, void 0, void 0, function* () {
            return Promise.all([
                categoryService_1.default.getAllExceptChildId(),
                tagService_1.default.getAll(),
                postService_1.default.getPostNotInId(dataItem, 3),
                commentService_1.default.getCommentSuggest()
            ])
                .then(([categoryAll, tag, postSuggest, commentSuggest]) => {
                return {
                    categoryAll,
                    tag,
                    postSuggest,
                    commentSuggest
                };
            });
        });
    }
    getHome(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            return Promise.all([
                categoryService_1.default.getCategoryAndPost(),
                categoryService_1.default.findOneCategoryAndGetPost(2, 5),
                categoryService_1.default.findOneCategoryAndGetPost(3, 4),
                categoryService_1.default.findOneCategoryAndGetPost(4, 4),
                categoryService_1.default.findOneCategoryAndGetPost(15, 4)
            ])
                .then(([rowFirst, rowSecond, rowThird, rowFour, rowFive]) => __awaiter(this, void 0, void 0, function* () {
                var dataIdArr = [];
                let tempArr = rowFirst.map((item) => {
                    return Number(item.post_categories[0].post_id);
                });
                dataIdArr.push(...tempArr);
                dataIdArr.push(...PostController.getIdProduct(rowSecond));
                dataIdArr.push(...PostController.getIdProduct(rowThird));
                dataIdArr.push(...PostController.getIdProduct(rowFour));
                dataIdArr.push(...PostController.getIdProduct(rowFive));
                const randomPost = yield postService_1.default.getPostNotInId(dataIdArr, 17);
                dataIdArr.push(...randomPost.map((item) => {
                    return Number(item.id);
                }));
                const dataNav = yield PostController.getNavData(dataIdArr);
                const rowRandom = randomPost.slice(0, 6);
                const randomFirst = randomPost.slice(6, 9);
                const randomSecond = randomPost.slice(9, 12);
                const randomSlider = randomPost.slice(12);
                return res.json({
                    rowFirst,
                    rowSecond,
                    rowThird,
                    rowFour,
                    rowFive,
                    rowRandom,
                    randomFirst,
                    randomSecond,
                    randomSlider,
                    dataNav: dataNav
                });
            }));
        });
    }
    getDetail(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataItem = yield postService_1.default.getOneBySlug((String(req.params.slug)));
            if (!dataItem) {
                return res.status(404).json({ message: 'Post not found' });
            }
            else {
                var dataIdInit = [Number(dataItem.id)];
                const postSuggestByCategory = yield postService_1.default.getPostSuggestByCategory(dataItem.categories[0].id, dataItem.id);
                dataIdInit.push(...postSuggestByCategory.map((item) => {
                    return Number(item.post_id);
                }));
                const postSuggestByTag = yield postService_1.default.getPostSuggestByTag(Number(dataItem.tags[0].id), dataIdInit);
                dataIdInit.push(...postSuggestByTag.map((item) => {
                    return Number(item.post_id);
                }));
                const dataNav = yield PostController.getNavData(dataIdInit);
                return res.json({ dataItem, postSuggestByCategory, postSuggestByTag, dataNav });
            }
        });
    }
    getPostByCategory(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let arrIdInit = [];
            const limit_item = 6;
            const slug = String(req.params.slug);
            const page = req.query.page ? Number(req.query.page) : 1;
            const category = yield categoryService_1.default.findOneBySlug(slug);
            if (!category)
                return res.status(404).json({ message: 'Category not found' });
            const result = yield postService_1.default.getPostByCategory(slug, page, limit_item);
            arrIdInit.push(...result.map((item) => {
                return Number(item.post_id);
            }));
            return Promise.all([
                postService_1.default.findAllProductByCategoryId(category.id),
                PostController.getNavData(arrIdInit)
            ])
                .then(([total, dataNav]) => {
                return res.json({
                    dataNav,
                    category,
                    result,
                    paginate: {
                        total_item: total.count,
                        current_page: page,
                        next_page: page < Math.ceil(total.count / limit_item) ? page + 1 : false,
                        prev_page: page - 1 <= 0 ? false : page - 1,
                        total_page: total.count / limit_item > 0 ? Math.ceil(total.count / limit_item) : 0,
                        limit_item: limit_item,
                        more_item: total.count - result.length > 0 ? total.count - result.length : false,
                    }
                });
            });
        });
    }
    getPostByTag(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let arrIdInit = [];
            const limit_item = 6;
            const slug = String(req.params.slug);
            const page = req.query.page ? Number(req.query.page) : 1;
            const tag = yield tagService_1.default.findOneBySlug(slug);
            if (!tag)
                return res.status(404).json({ message: 'Category not found' });
            const result = yield postService_1.default.getPostByTag(slug, page, limit_item);
            arrIdInit.push(...result.map((item) => {
                return Number(item.post_id);
            }));
            return Promise.all([
                postService_1.default.findAllProductByTagId(tag.id),
                PostController.getNavData(arrIdInit)
            ])
                .then(([total, dataNav]) => {
                return res.json({
                    dataNav,
                    tag,
                    result,
                    paginate: {
                        total_item: total.count,
                        current_page: page,
                        next_page: page < Math.ceil(total.count / limit_item) ? page + 1 : false,
                        prev_page: page - 1 <= 0 ? false : page - 1,
                        total_page: total.count / limit_item > 0 ? Math.ceil(total.count / limit_item) : 0,
                        limit_item: limit_item,
                        more_item: total.count - result.length > 0 ? total.count - result.length : false,
                    }
                });
            });
        });
    }
    getByAutoComplete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataItem = yield postService_1.default.getByAutoComplete(String(req.query.key));
            return res.status(200).json(dataItem);
        });
    }
    getBySearch(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let arrIdInit = [];
            const limit_item = 6;
            const page = req.query.page ? Number(req.query.page) : 1;
            const result = yield postService_1.default.getPostByTitle(String(req.query.key), page, limit_item);
            arrIdInit.push(...result.map((item) => {
                return Number(item.id);
            }));
            return Promise.all([
                postService_1.default.getAllPostByTitle(String(req.query.key)),
                PostController.getNavData(arrIdInit)
            ])
                .then(([total, dataNav]) => {
                return res.json({
                    dataNav,
                    result,
                    paginate: {
                        total_item: total.count,
                        current_page: page,
                        next_page: page < Math.ceil(total.count / limit_item) ? page + 1 : false,
                        prev_page: page - 1 <= 0 ? false : page - 1,
                        total_page: total.count / limit_item > 0 ? Math.ceil(total.count / limit_item) : 0,
                        limit_item: limit_item,
                        more_item: total.count - result.length > 0 ? total.count - result.length : false,
                    }
                });
            });
        });
    }
    getAllComments(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataItem = yield postService_1.default.getAllCommentsById(Number(req.params.id));
            if (dataItem)
                return res.status(200).json(dataItem);
            return res.status(200).json([]);
        });
    }
}
exports.default = new PostController;
