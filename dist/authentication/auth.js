"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const userService_1 = __importDefault(require("../services/userService"));
const tokenService_1 = __importDefault(require("../services/tokenService"));
const bcrypt_1 = require("bcrypt");
class Authentication {
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield userService_1.default.login(req.body.email)
                .then((user) => __awaiter(this, void 0, void 0, function* () {
                if (!user)
                    return res.status(200).json({ status: "error", message: "Email chưa được đăng ký" });
                const checkPassWord = yield (0, bcrypt_1.compare)(req.body.password, user.password);
                if (!checkPassWord)
                    return res.status(200).json({ status: "error", message: "Mật khẩu không chính xác !" });
                const { id, name, email, role } = user;
                const refresh_token = tokenService_1.default.generateRefreshToken({ id, name, email, role });
                const access_token = tokenService_1.default.generateAccessToken({ id, name, email, role });
                yield userService_1.default.findByIdAndUpdateToken(user.id, { remember_token: refresh_token });
                return res.status(200).json({ status: 'success', refresh_token, access_token });
            }));
        });
    }
    logout(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.body.refresh_token;
                const idUser = tokenService_1.default.verifyRefreshTokenUser(token);
                yield userService_1.default.findByIdAndUpdateToken(idUser, { remember_token: '' });
                return res.status(200).json({ status: 'success', message: 'logout success' });
            }
            catch (_a) {
                return res.status(200).json({ status: 'error', message: 'token invalid or logout before' });
            }
        });
    }
    register(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let payload = Object.assign(Object.assign({}, req.body), { role: 0, login_type: 1 });
            try {
                const dataInsert = yield userService_1.default.insert(payload);
                const { id, name, email, role } = dataInsert;
                const refresh_token = tokenService_1.default.generateRefreshToken({ id, name, email, role });
                const access_token = tokenService_1.default.generateAccessToken({ id, name, email, role });
                yield userService_1.default.findByIdAndUpdateToken(dataInsert.id, { remember_token: refresh_token });
                return res.status(200).json({ status: 'success', refresh_token, access_token });
            }
            catch (error) {
                return res.status(500).json({ status: 'error', error });
            }
        });
    }
}
exports.default = new Authentication;
