"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
class ValidationRequest {
    static validateResult(validation, res, next) {
        if (validation.error) {
            return res.status(400).json({ error: validation.error.details[0].message });
        }
        else {
            next();
        }
    }
}
ValidationRequest.handleTagId = { tag_id: joi_1.default.number().required().messages({
        'number.base': 'Tag id must be a number.',
        'number.empty': 'Tag id is required.',
        'any.required': 'Tag id is required.',
    }) };
ValidationRequest.handlePostId = { post_id: joi_1.default.number().required().messages({
        'number.base': 'Post id must be a number.',
        'number.empty': 'Post id is required.',
        'any.required': 'Post id is required.',
    }) };
ValidationRequest.handleCategoryId = { category_id: joi_1.default.number().required().messages({
        'number.base': 'Category id must be a number.',
        'number.empty': 'Category id is required.',
        'any.required': 'Category id is required.',
    }) };
ValidationRequest.handleParentId = { parent_id: joi_1.default.number().required().messages({
        'number.base': 'Parent id must be a number.',
        'number.empty': 'Parent id is required.',
        'any.required': 'Parent id is required.',
    }) };
ValidationRequest.handleUserId = { user_id: joi_1.default.number().required().messages({
        'number.base': 'Author id must be a number.',
        'number.empty': 'Author id is required.',
        'any.required': 'Author id required.',
    }) };
ValidationRequest.handleName = { name: joi_1.default.string().required().messages({
        'string.empty': 'Name is required.',
        'any.required': 'Name is required.',
    }) };
ValidationRequest.handleDescription = { description: joi_1.default.string().required().messages({
        'string.empty': 'Description is required.',
        'any.required': 'Description is required.',
    }) };
ValidationRequest.handleContent = { content: joi_1.default.string().required().messages({
        'string.empty': 'Content is required.',
        'any.required': 'Content is required.',
    }) };
ValidationRequest.handleTitle = { title: joi_1.default.string().required().messages({
        'string.empty': 'Title is required.',
        'any.required': 'Title is required.',
    }) };
ValidationRequest.handleKey = { key: joi_1.default.string().required().messages({
        'string.empty': 'Key is required.',
        'any.required': 'Key is required.',
    }) };
ValidationRequest.handleActive = { active: joi_1.default.number().min(0).max(1).required().messages({
        'number.empty': 'Active is required.',
        'any.required': 'Active is required.',
        'number.base': 'Active must be a number.',
        'number.max': 'Active  must be a number 0 or 1',
        'number.min': 'Active  must be a number 0 or 1',
    }) };
ValidationRequest.handleEmail = { email: joi_1.default.string().email().required().messages({
        'string.email': 'Invalid email format.',
        'string.empty': 'Email is required.',
        'any.required': 'Email is required.',
    }) };
ValidationRequest.handlePassword = { password: joi_1.default.string().required().min(6).max(20).messages({
        'string.empty': 'Password is required.',
        'any.required': 'Password is required.',
        'string.max': 'Password length must be less than or equal to 20 characters long',
        'string.min': 'password length must be at least 6 characters long',
    }) };
ValidationRequest.handleRole = { role: joi_1.default.number().required().messages({
        'number.base': 'Role must be a number.',
        'number.empty': 'Role is required.',
        'any.required': 'Role is required.',
    }) };
ValidationRequest.handleRefreshToken = { refresh_token: joi_1.default.string().min(10).required().messages({
        'string.empty': 'Refresh token is required.',
        'any.required': 'Refresh token is required.',
        'string.min': 'Refresh token must be at least {#limit} characters long.',
    }) };
ValidationRequest.handleLoginType = { login_type: joi_1.default.number().min(1).max(3).required().messages({
        'number.base': 'Login type must be a number.',
        'number.empty': 'Login type is required.',
        'any.required': 'Login type is required.',
        'number.max': 'Login type  must be a number from 1 to 3',
        'number.min': 'Login type  must be a number from 1 to 3',
    }) };
ValidationRequest.handleImage = {
    image: joi_1.default.object({
        data: joi_1.default.binary().required(),
        mimetype: joi_1.default.string().valid('image/jpeg', 'image/png', 'image/gif', 'image/jpg').required(),
    }).required(),
};
exports.default = ValidationRequest;
