"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const validationRequest_1 = __importDefault(require("./validationRequest"));
class ValidationPost extends validationRequest_1.default {
    validateBase(req, res, next) {
        const schema = joi_1.default.object(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, ValidationPost.handleTitle), ValidationPost.handleUserId), ValidationPost.handleDescription), ValidationPost.handleActive), ValidationPost.handleContent), ValidationPost.handleCategoryId), ValidationPost.handleTagId));
        return ValidationPost.validateResult(schema.validate(req.body), res, next);
    }
    validateUpdate(req, res, next) {
        const schema = joi_1.default.object(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, ValidationPost.handleTitle), ValidationPost.handleDescription), ValidationPost.handleActive), ValidationPost.handleContent), ValidationPost.handleCategoryId), ValidationPost.handleTagId));
        return ValidationPost.validateResult(schema.validate(req.body), res, next);
    }
    validateKeySearch(req, res, next) {
        const schema = joi_1.default.object(Object.assign({}, ValidationPost.handleKey));
        return ValidationPost.validateResult(schema.validate(req.query), res, next);
    }
}
exports.default = new ValidationPost;
