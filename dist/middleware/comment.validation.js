"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const validationRequest_1 = __importDefault(require("./validationRequest"));
class ValidationComment extends validationRequest_1.default {
    validateBase(req, res, next) {
        const schema = joi_1.default.object(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, ValidationComment.handleActive), ValidationComment.handleParentId), ValidationComment.handleUserId), ValidationComment.handleName), ValidationComment.handleEmail), ValidationComment.handleContent), ValidationComment.handlePostId));
        return ValidationComment.validateResult(schema.validate(req.body), res, next);
    }
}
exports.default = new ValidationComment;
