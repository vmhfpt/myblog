"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const validationRequest_1 = __importDefault(require("./validationRequest"));
class ValidationUser extends validationRequest_1.default {
    validateBase(req, res, next) {
        const schema = joi_1.default.object(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, ValidationUser.handleName), ValidationUser.handleEmail), ValidationUser.handlePassword), ValidationUser.handleRole), ValidationUser.handleLoginType));
        return ValidationUser.validateResult(schema.validate(req.body), res, next);
    }
    validateLogin(req, res, next) {
        const schema = joi_1.default.object(Object.assign(Object.assign({}, ValidationUser.handleEmail), ValidationUser.handlePassword));
        return ValidationUser.validateResult(schema.validate(req.body), res, next);
    }
    validateRegister(req, res, next) {
        const schema = joi_1.default.object(Object.assign(Object.assign(Object.assign({}, ValidationUser.handleEmail), ValidationUser.handlePassword), ValidationUser.handleName));
        return ValidationUser.validateResult(schema.validate(req.body), res, next);
    }
    validateRefreshToken(req, res, next) {
        const schema = joi_1.default.object(Object.assign({}, ValidationUser.handleRefreshToken));
        return ValidationUser.validateResult(schema.validate(req.body), res, next);
    }
}
exports.default = new ValidationUser;
