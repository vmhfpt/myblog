"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = require("jsonwebtoken");
require("dotenv/config");
function authLogin(role = 'admin') {
    return (req, res, next) => {
        //   next();
        //   return true;
        const authHeader = req.header('authorization');
        const token = authHeader && authHeader.split(' ')[1];
        if (token == null) {
            return res.status(403).json({ status: 'error', message: 'Token is empty or invalid' });
        }
        try {
            const decoded = (0, jsonwebtoken_1.verify)(token, process.env.ACCESS_TOKEN_SECRET);
            if (decoded.data.role == 0 && role == 'admin') {
                return res.status(403).json({ status: 'error', message: 'You don`t have authorization' });
            }
            next();
        }
        catch (_a) {
            return res.status(401).json({ status: 'error', message: 'Unauthorized' });
        }
    };
}
exports.default = authLogin;
