"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const post_1 = require("../models/post/post");
const user_1 = require("../models/user/user");
const tag_1 = require("../models/tag/tag");
const post_meta_1 = require("../models/post_meta/post_meta");
const category_1 = require("../models/category/category");
const sequelize_1 = require("sequelize");
const post_category_1 = require("../models/post_category/post_category");
const post_tag_1 = require("../models/post_tag/post_tag");
const comment_1 = require("../models/comment/comment");
class PostService {
    getPostNotInId(dataItem, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_1.Post.findAll({
                limit: limit,
                order: [['id', 'DESC']],
                attributes: ['createdAt', 'title', 'thumb', 'slug', 'id'],
                where: {
                    id: {
                        [sequelize_1.Op.notIn]: dataItem,
                    },
                },
            });
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_1.Post.findAll({
                include: [
                    {
                        model: tag_1.Tag,
                        attributes: ['id', 'title', 'slug'],
                    },
                    {
                        model: post_meta_1.PostMeta
                    },
                    {
                        model: category_1.Category,
                        attributes: ['title', 'slug', 'id'],
                        through: {}
                    },
                    {
                        model: user_1.User,
                        attributes: ['name', 'id']
                    }
                ],
                order: [["id", "DESC"]],
            });
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const post = yield post_1.Post.findByPk(id);
            if (post) {
                yield post_1.Post.update(payload, {
                    where: {
                        id: id
                    }
                });
                return post;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_1.Post.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let dataItem = yield post_1.Post.findOne({
                where: { id: id },
                include: [
                    {
                        model: tag_1.Tag,
                        attributes: ['title', 'slug', 'id'],
                    },
                    {
                        model: post_meta_1.PostMeta
                    },
                    {
                        model: category_1.Category,
                        attributes: ['title', 'slug', 'id'],
                        through: {}
                    },
                    {
                        model: user_1.User,
                        attributes: ['name', 'id']
                    }
                ],
            });
            if (dataItem) {
                return dataItem;
            }
            return null;
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const post = yield post_1.Post.findByPk(id);
            if (post) {
                yield post_1.Post.destroy({
                    where: {
                        id: id
                    }
                });
                return post;
            }
            return null;
        });
    }
    getAllCommentsById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let dataItem = yield comment_1.Comment.findAll({
                where: { post_id: id, parent_id: 0 },
                include: [
                    {
                        model: comment_1.Comment,
                        as: 'child_comments',
                        required: false,
                        order: [['id', 'DESC']],
                    },
                    // how to order by id in here
                ],
                order: [['id', 'DESC']],
                // how to order by id in here
            });
            if (dataItem.length != 0) {
                return dataItem;
            }
            return null;
        });
    }
    getOneBySlug(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            let dataItem = yield post_1.Post.findOne({
                where: { slug: slug },
                include: [
                    {
                        model: tag_1.Tag,
                        attributes: ['title', 'slug', 'id'],
                    },
                    {
                        model: post_meta_1.PostMeta
                    },
                    {
                        model: category_1.Category,
                        attributes: ['title', 'slug', 'id'],
                        through: {}
                    },
                    {
                        model: user_1.User,
                        attributes: ['name', 'id']
                    },
                ],
            });
            if (dataItem) {
                return dataItem;
            }
            return null;
        });
    }
    getAllPost() {
        return __awaiter(this, void 0, void 0, function* () {
            let dataItem = yield post_1.Post.findAll({
                attributes: ['slug']
            });
            if (dataItem) {
                return dataItem;
            }
            return null;
        });
    }
    getPostSuggestByCategory(category_id, post_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findAll({
                where: {
                    [sequelize_1.Op.and]: [{ category_id: category_id }, { post_id: { [sequelize_1.Op.ne]: post_id } }],
                },
                include: {
                    model: post_1.Post,
                    attributes: ['title', 'description', 'createdAt', 'slug', 'thumb']
                },
                offset: 0,
                limit: 3,
                order: [['id', 'DESC']],
            });
        });
    }
    getPostSuggestByTag(tag_id, arrayId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_tag_1.PostTag.findAll({
                where: {
                    [sequelize_1.Op.and]: [{ tag_id: tag_id }, { post_id: { [sequelize_1.Op.notIn]: arrayId } }],
                },
                include: {
                    model: post_1.Post,
                    attributes: ['title', 'description', 'createdAt', 'slug', 'thumb']
                },
                offset: 0,
                limit: 6,
                order: [['id', 'DESC']],
            });
        });
    }
    getPostByCategory(slug, page, limit_item) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findAll({
                include: [
                    {
                        model: category_1.Category,
                        where: {
                            slug: slug
                        },
                        attributes: ['id']
                    },
                    {
                        model: post_1.Post,
                        attributes: ['title', 'description', 'createdAt', 'slug', 'thumb'],
                        include: [{
                                model: user_1.User,
                                attributes: ['name']
                            }]
                    },
                ],
                offset: 0,
                limit: page * limit_item,
                order: [['id', 'DESC']],
            });
        });
    }
    getPostByTag(slug, page, limit_item) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_tag_1.PostTag.findAll({
                include: [
                    {
                        model: tag_1.Tag,
                        where: {
                            slug: slug
                        },
                        attributes: ['id']
                    },
                    {
                        model: post_1.Post,
                        attributes: ['title', 'description', 'createdAt', 'slug', 'thumb'],
                        include: [{
                                model: user_1.User,
                                attributes: ['name']
                            }]
                    },
                ],
                offset: 0,
                limit: page * limit_item,
                order: [['id', 'DESC']],
            });
        });
    }
    findAllProductByTagId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_tag_1.PostTag.findAndCountAll({
                where: {
                    tag_id: id,
                },
            });
        });
    }
    findAllProductByCategoryId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findAndCountAll({
                where: {
                    category_id: id,
                },
            });
        });
    }
    getByAutoComplete(key) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_1.Post.findAll({
                where: {
                    title: { [sequelize_1.Op.substring]: key }
                },
                offset: 0,
                limit: 5,
                order: [['id', 'DESC']],
                attributes: ['title', 'slug']
            });
        });
    }
    getAllPostByTitle(key) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_1.Post.findAndCountAll({
                where: {
                    title: { [sequelize_1.Op.substring]: key }
                },
            });
        });
    }
    getPostByTitle(key, page, limit_item) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_1.Post.findAll({
                include: [
                    {
                        model: user_1.User,
                        attributes: ['name']
                    }
                ],
                where: {
                    title: { [sequelize_1.Op.substring]: key }
                },
                offset: 0,
                limit: page * limit_item,
                order: [['id', 'DESC']],
                attributes: ['id', 'title', 'description', 'createdAt', 'slug', 'thumb']
            });
        });
    }
}
exports.default = new PostService;
