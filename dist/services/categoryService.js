"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const category_1 = require("../models/category/category");
const post_1 = require("../models/post/post");
const post_category_1 = require("../models/post_category/post_category");
const user_1 = require("../models/user/user");
const sequelize_1 = __importDefault(require("sequelize"));
class CategoryService {
    getAllExceptChildId() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findAll({
                attributes: [
                    [sequelize_1.default.fn('COUNT', sequelize_1.default.col('category_id')), 'total'],
                ],
                include: {
                    model: category_1.Category,
                    attributes: ['title', 'slug', 'id'],
                },
                group: ['`post_categories`.`category_id`'],
            });
        });
    }
    getCategoryAndPost() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield category_1.Category.findAll({
                include: [
                    {
                        model: post_category_1.PostCategory,
                        include: [{
                                model: post_1.Post,
                                attributes: ['createdAt', 'title', 'description', 'thumb', 'slug'],
                                include: [{
                                        model: user_1.User,
                                        attributes: ['name'],
                                    }]
                            }],
                        limit: 1,
                        order: [['id', 'DESC']],
                    },
                ],
                attributes: ['title', 'slug']
            });
        });
    }
    findOneCategoryAndGetPost(category_id, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findAll({
                offset: 1,
                limit: limit,
                where: {
                    category_id: category_id
                },
                include: [
                    {
                        model: post_1.Post,
                        attributes: ['createdAt', 'title', 'description', 'thumb', 'slug'],
                        include: [{
                                model: user_1.User,
                                attributes: ['name'],
                            }]
                    },
                    {
                        model: category_1.Category,
                        attributes: ['id', 'title', 'slug'],
                    }
                ],
                order: [['id', 'DESC']],
            });
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield category_1.Category.findAll({
                include: {
                    model: category_1.Category,
                    attributes: ['title', 'id']
                }
            });
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const category = yield category_1.Category.findByPk(id);
            if (category) {
                yield category_1.Category.update(payload, {
                    where: {
                        id: id
                    }
                });
                return category;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield category_1.Category.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield category_1.Category.findOne({ where: { id: id } });
        });
    }
    findOneBySlug(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield category_1.Category.findOne({
                where: {
                    slug: slug
                },
                attributes: ['id', 'title', 'slug'],
            });
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const category = yield category_1.Category.findByPk(id);
            if (category) {
                yield category_1.Category.destroy({
                    where: {
                        id: id
                    }
                });
                return category;
            }
            return null;
        });
    }
}
exports.default = new CategoryService;
