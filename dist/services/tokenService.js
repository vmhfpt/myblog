"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = require("jsonwebtoken");
require("dotenv/config");
const userService_1 = __importDefault(require("./userService"));
class TokenService {
    generateRefreshToken(data) {
        const refresh_token = (0, jsonwebtoken_1.sign)({
            data: data
        }, TokenService.refreshTokenSecret, { expiresIn: TokenService.timeRefreshTokenExpired });
        return refresh_token;
    }
    generateAccessToken(data) {
        const access_token = (0, jsonwebtoken_1.sign)({
            data: data
        }, TokenService.accessTokenSecret, { expiresIn: TokenService.timeAccessTokenExpired });
        return access_token;
    }
    verifyTokenUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const dataUser = yield userService_1.default.findOneById(id);
                if (dataUser) {
                    const tokenVerify = dataUser.remember_token;
                    if (!tokenVerify)
                        throw new Error('unauthoziration');
                    (0, jsonwebtoken_1.verify)(tokenVerify, TokenService.refreshTokenSecret);
                }
            }
            catch (error) {
                throw new Error('unauthoziration');
            }
        });
    }
    verifyRefreshTokenUser(refreshToken) {
        const decoded = (0, jsonwebtoken_1.verify)(refreshToken, TokenService.refreshTokenSecret);
        return decoded.data.id;
    }
    getNewAccessToken(refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const decoded = (0, jsonwebtoken_1.verify)(refreshToken, TokenService.refreshTokenSecret);
                const { id, name, email, role } = decoded.data;
                yield this.verifyTokenUser(id);
                const access_token = this.generateAccessToken({ id, name, email, role });
                const refresh_token = this.generateRefreshToken({ id, name, email, role });
                return ({ status: 'success', access_token, refresh_token });
            }
            catch (_a) {
                return ({ status: 'error', message: 'refresh_token error' });
            }
        });
    }
}
TokenService.refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET;
TokenService.timeRefreshTokenExpired = process.env.TIME_REFRESH_TOKEN_EXPIRED;
TokenService.accessTokenSecret = process.env.ACCESS_TOKEN_SECRET;
TokenService.timeAccessTokenExpired = process.env.TIME_ACCESS_TOKEN_EXPIRED;
exports.default = new TokenService;
