"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const post_category_1 = require("../models/post_category/post_category");
class PostCategoryService {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findAll({});
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const post = yield post_category_1.PostCategory.findByPk(id);
            if (post) {
                yield post_category_1.PostCategory.update(payload, {
                    where: {
                        id: id
                    }
                });
                return post;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_category_1.PostCategory.findOne({ where: { id: id } });
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const post = yield post_category_1.PostCategory.findByPk(id);
            if (post) {
                yield post_category_1.PostCategory.destroy({
                    where: {
                        id: id
                    }
                });
                return post;
            }
            return null;
        });
    }
}
exports.default = new PostCategoryService;
