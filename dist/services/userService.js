"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../models/user/user");
class UserService {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield user_1.User.findAll({});
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield user_1.User.findByPk(id);
            if (user) {
                user.name = payload.name;
                user.email = payload.email;
                user.role = payload.role;
                user.login_type = payload.login_type;
                user.password = payload.password;
                yield user.save();
                return user;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield user_1.User.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield user_1.User.findOne({ where: { id: id } });
        });
    }
    login(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield user_1.User.findOne({ where: { email: email } });
        });
    }
    findByIdAndUpdateToken(id, payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield user_1.User.findByPk(id);
            if (user) {
                yield user_1.User.update(payload, {
                    where: {
                        id: id
                    }
                });
                return user;
            }
            return null;
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield user_1.User.findByPk(id);
            if (user) {
                yield user_1.User.destroy({
                    where: {
                        id: id
                    }
                });
                return user;
            }
            return null;
        });
    }
}
exports.default = new UserService;
