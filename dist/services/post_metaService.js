"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const post_meta_1 = require("../models/post_meta/post_meta");
class PostMetaService {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_meta_1.PostMeta.findAll({});
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const postMeta = yield post_meta_1.PostMeta.findByPk(id);
            if (post_meta_1.PostMeta) {
                yield post_meta_1.PostMeta.update(payload, {
                    where: {
                        id: id
                    }
                });
                return postMeta;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_meta_1.PostMeta.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_meta_1.PostMeta.findOne({ where: { id: id } });
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const postMeta = yield post_meta_1.PostMeta.findByPk(id);
            if (post_meta_1.PostMeta) {
                yield post_meta_1.PostMeta.destroy({
                    where: {
                        id: id
                    }
                });
                return postMeta;
            }
            return null;
        });
    }
}
exports.default = new PostMetaService;
