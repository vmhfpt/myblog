"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tag_1 = require("../models/tag/tag");
class TagService {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield tag_1.Tag.findAll({});
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tag = yield tag_1.Tag.findByPk(id);
            if (tag) {
                yield tag_1.Tag.update(payload, {
                    where: {
                        id: id
                    }
                });
                return tag;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield tag_1.Tag.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield tag_1.Tag.findOne({ where: { id: id } });
        });
    }
    findOneBySlug(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield tag_1.Tag.findOne({ where: { slug: slug } });
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tag = yield tag_1.Tag.findByPk(id);
            if (tag) {
                yield tag_1.Tag.destroy({
                    where: {
                        id: id
                    }
                });
                return tag;
            }
            return null;
        });
    }
}
exports.default = new TagService;
