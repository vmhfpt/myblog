"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const post_tag_1 = require("../models/post_tag/post_tag");
const tag_1 = require("../models/tag/tag");
const post_1 = require("../models/post/post");
const user_1 = require("../models/user/user");
class PostTagService {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            // return await PostTag.findAll({});
            const result = yield post_tag_1.PostTag.findAll({
                include: [
                    {
                        model: tag_1.Tag,
                        attributes: ['id', 'title']
                    },
                    {
                        model: post_1.Post,
                        attributes: ['title', 'description', 'createdAt', 'slug', 'thumb'],
                        include: [
                            {
                                model: user_1.User, //error Object literal may only specify known properties, and 'model' does not exist in type 'Includeable[]'. in typescirpt
                                attributes: ['name']
                            }
                        ]
                    }
                ],
                order: [['id', 'DESC']],
            });
            return result;
        });
    }
    findOneAndUpdate(payload, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const postTag = yield post_tag_1.PostTag.findByPk(id);
            if (postTag) {
                yield post_tag_1.PostTag.update(payload, {
                    where: {
                        id: id
                    }
                });
                return postTag;
            }
            return null;
        });
    }
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_tag_1.PostTag.create(payload);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield post_tag_1.PostTag.findOne({ where: { id: id } });
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const postTag = yield post_tag_1.PostTag.findByPk(id);
            if (postTag) {
                yield post_tag_1.PostTag.destroy({
                    where: {
                        id: id
                    }
                });
                return postTag;
            }
            return null;
        });
    }
}
exports.default = new PostTagService;
