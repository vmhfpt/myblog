"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const comment_1 = require("../models/comment/comment");
const post_1 = require("../models/post/post");
class CommentService {
    insert(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield comment_1.Comment.create(payload);
        });
    }
    getCommentSuggest() {
        return __awaiter(this, void 0, void 0, function* () {
            return comment_1.Comment.findAll({
                offset: 0,
                limit: 3,
                include: {
                    model: post_1.Post,
                    attributes: ['slug']
                },
                order: [['id', 'DESC']],
                attributes: ['name', 'content', 'user_id']
            });
        });
    }
}
exports.default = new CommentService;
