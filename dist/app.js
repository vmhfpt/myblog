"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routers_1 = __importDefault(require("./routers"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const app = (0, express_1.default)();
// const corsOptions = {
//   origin: 'http://localhost:3006',
//   optionsSuccessStatus: 200,
//   credentials: true, // some legacy browsers (IE11, various SmartTVs) choke on 204
// };
app.use((0, cors_1.default)());
const port = 5000;
app.use(express_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use(body_parser_1.default.json());
(0, routers_1.default)(app);
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
