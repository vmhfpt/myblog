"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sequelize = void 0;
const database_1 = __importDefault(require("./database"));
const sequelize_1 = require("sequelize");
const setupEnv = database_1.default.development;
exports.sequelize = new sequelize_1.Sequelize(setupEnv.database, setupEnv.username, setupEnv.password, {
    host: setupEnv.host,
    dialect: setupEnv.dialect,
});
