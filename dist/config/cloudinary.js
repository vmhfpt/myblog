"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteImage = exports.uploadImage = void 0;
require("dotenv/config");
const cloudinary_1 = __importDefault(require("cloudinary"));
cloudinary_1.default.v2.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
    secure: true
});
const uploadImage = (imagePath, folder = "post") => __awaiter(void 0, void 0, void 0, function* () {
    if (!imagePath)
        return { status: 'error', message: 'There is no image upload' };
    const options = {
        use_filename: true,
        unique_filename: false,
        overwrite: true,
        folder: folder
    };
    const result = yield cloudinary_1.default.v2.uploader.upload(imagePath, options);
    let convertedUrl = result.url.replace("http://", "https://");
    return { status: 'success', url: convertedUrl };
});
exports.uploadImage = uploadImage;
const deleteImage = (url, file = true, folder) => __awaiter(void 0, void 0, void 0, function* () {
    if (!file || url == 'https://laptrinhcuocsong.com/images/thumbnail_default.png')
        return { status: 'error', message: 'There is no image upload' };
    const public_id = url.slice(url.indexOf(folder), url.lastIndexOf("."));
    const deleteImage = yield cloudinary_1.default.v2.uploader.destroy(public_id);
    return deleteImage;
});
exports.deleteImage = deleteImage;
