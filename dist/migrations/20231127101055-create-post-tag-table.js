'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('post_tags', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      post_id : Sequelize.DataTypes.INTEGER,
      tag_id : Sequelize.DataTypes.INTEGER,
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('post_tags');
  }
};
