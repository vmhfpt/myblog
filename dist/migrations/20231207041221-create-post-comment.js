'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('comments', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      user_id : Sequelize.DataTypes.INTEGER,
      post_id : Sequelize.DataTypes.INTEGER,
      parent_id : Sequelize.DataTypes.INTEGER,
      name : { type: Sequelize.DataTypes.STRING(256)},
      email : Sequelize.DataTypes.STRING(256),
      content : Sequelize.DataTypes.STRING(200),
      active : Sequelize.DataTypes.INTEGER,
      createdAt : {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      },
      updatedAt : {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      }
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('comments');
  }
};
