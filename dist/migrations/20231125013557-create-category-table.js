'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('categories', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      parent_id : Sequelize.DataTypes.INTEGER,
      title : { type: Sequelize.DataTypes.STRING(256), unique: true },
      slug : Sequelize.DataTypes.STRING(256),
      //content : Sequelize.DataTypes.TEXT('long'),
      active : Sequelize.DataTypes.INTEGER,
      createdAt : {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      },
      updatedAt : {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      }
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('categories');
  }
};
