'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      role : Sequelize.DataTypes.INTEGER,
      name: Sequelize.DataTypes.STRING(256),
      google_id: Sequelize.DataTypes.STRING(256),
      facebook_id: Sequelize.DataTypes.STRING(256),
      email : Sequelize.DataTypes.STRING(256),
      password : Sequelize.DataTypes.STRING(500),
      login_type : Sequelize.DataTypes.INTEGER,
      remember_token : Sequelize.DataTypes.STRING,
      createdAt : {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      },
      updatedAt : {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      }
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};
