import 'dotenv/config';
import { Request, Response } from 'express';
import userService from "../services/userService";
import tokenService from "../services/tokenService";
import { compare } from 'bcrypt';
import { UserAttributes } from '../models/user/user.interface';
class Authentication {
    public async login(req : Request, res : Response){
        return await userService.login(req.body.email)
        .then(async (user) => {
            if (!user) return res.status(200).json({ status: "error", message : "Email chưa được đăng ký" });
            const checkPassWord =  await compare(req.body.password, user.password);
            if (!checkPassWord) return res.status(200).json({ status :"error", message: "Mật khẩu không chính xác !" });
            const {id, name, email, role} = user;
            const refresh_token = tokenService.generateRefreshToken({id, name, email, role});
            const access_token = tokenService.generateAccessToken({id, name, email, role});
    
            await userService.findByIdAndUpdateToken(user.id, { remember_token: refresh_token } );
            return res.status(200).json({status : 'success', refresh_token, access_token});
       })
    }
    public async logout(req : Request, res : Response){
        
        try {
            const token = req.body.refresh_token;
            const idUser : number = tokenService.verifyRefreshTokenUser(token);
            await userService.findByIdAndUpdateToken(idUser, { remember_token: '' });
            return  res.status(200).json({status : 'success', message : 'logout success'});
         }catch {
            return  res.status(200).json({status : 'error', message : 'token invalid or logout before'});
         } 
    }
    public async register(req : Request, res : Response){
        let payload : UserAttributes = {
            ...req.body,
            role : 0,
            login_type : 1
        }
        try {
            const dataInsert = await userService.insert(payload);
            const {id, name, email,role} = dataInsert;
            const refresh_token = tokenService.generateRefreshToken({id, name, email, role});
            const access_token = tokenService.generateAccessToken({id, name, email, role});
            await userService.findByIdAndUpdateToken(dataInsert.id, { remember_token: refresh_token });
            return res.status(200).json({status : 'success', refresh_token, access_token});
        } catch (error) {
            return res.status(500).json({status : 'error' , error});
        }
    }
}
export default new Authentication;

