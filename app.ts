import express from 'express';
import useRoute from './routers';
import bodyParser from 'body-parser';
import cors from 'cors';
const app = express();
// const corsOptions = {
//   origin: 'http://localhost:3006',
//   optionsSuccessStatus: 200,
//   credentials: true, // some legacy browsers (IE11, various SmartTVs) choke on 204
// };
 
app.use(cors()); 

const port = 3000 ;
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.get('/test', (req, res) => {
  return res.json({status : 'test success change'});
});

useRoute(app);
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
/**sudo chown root:root -R /root/.ssh/
sudo chmod 700 /root/.ssh/
sudo chmod 600 /root/.ssh/authorized_keys */