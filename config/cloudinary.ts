import 'dotenv/config';
import cloudinary from 'cloudinary';
cloudinary.v2.config({ 
    cloud_name: process.env.CLOUD_NAME, 
    api_key: process.env.CLOUDINARY_API_KEY, 
    api_secret: process.env.CLOUDINARY_API_SECRET,
    secure: true
});

export const uploadImage = async (imagePath : string | boolean, folder : string = "post") => {
    if(!imagePath) return {status : 'error', message : 'There is no image upload'};
    const options = {
        use_filename: true,
        unique_filename: false,
        overwrite: true,
        folder: folder
    };
    const result = await cloudinary.v2.uploader.upload(imagePath as string, options);
    let convertedUrl : string = result.url.replace("http://", "https://");
    return {status : 'success', url : convertedUrl};
};
export const deleteImage = async(url : string, file : boolean = true, folder : string) => {
    if(!file || url == 'https://laptrinhcuocsong.com/images/thumbnail_default.png') return {status : 'error', message : 'There is no image upload'};
    
    const public_id = url.slice(
        url.indexOf(folder),
        url.lastIndexOf(".")
    );
    const deleteImage = await cloudinary.v2.uploader.destroy(public_id);
    return deleteImage;
}
