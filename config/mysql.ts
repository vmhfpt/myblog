import config from "./database";
import { Sequelize } from 'sequelize';
interface SetUp {
    username: string | undefined;
    password: string | undefined;
    database: string | undefined;
    host: string | undefined;
    dialect: string;
}
const setupEnv : SetUp = config.development;

export const sequelize : Sequelize = new Sequelize(setupEnv.database as string, setupEnv.username as string, setupEnv.password as string, {
    host: setupEnv.host,
    dialect: setupEnv.dialect as any,
});